#
# Messages
# -
messages:
    #
    # Error Messages
    # -
    errors:
        #
        # Incorrect Command Usage
        # -
        # The "incorrect-command-usage" message is shown to the
        # player if the player types an command incorrectly.
        #
        incorrect-command-usage: "§cIncorrect command usage."

        #
        # Player Only Command
        # -
        # The "player-only-command" message is shown to the
        # command sender if the sender is not an instance
        # of a player.
        #
        player-only-command: "§cYou must be a player in order to execute that command."

        #
        # No Permission
        # -
        # The "no-permission" message is displayed to the command sender if
        # the sender does not have the required permissions to operate the
        # specified command.
        #
        no-permission: "§cYou do not have permission."

        #
        # Cannot Join Game In Queue
        # -
        # The "cannot-join-game-in-queue" message is displayed to the
        # player when the player attempts to join a game whilst
        # they're already queuing for another game.
        #
        cannot-join-game-in-queue: "§cYou cannot join a game and queue the same time."

        #
        # Lobby Error Messages
        # -
        lobby:
            #
            # Not Found
            # -
            # The "not-found" message is displayed to the player
            # if the lobby could not be found. This is usually
            # due to an internal error.
            #
            not-found: "§cThe lobby could not be found."

            #
            # Already In Lobby
            # -
            # The "already-in-lobby" message is displayed to the player
            # if the player attempts to join the lobby whilst already
            # in the lobby.
            #
            already-in-lobby: "§cYou are already within the lobby."

            #
            # Not In Lobby
            # -
            # The "not-in-lobby" message is displayed to the player
            # if the player attempts to leave the lobby whilst not
            # being in the lobby.
            #
            not-in-lobby: "§cYou are not within the lobby."

        #
        # Arena Error Messages
        # -
        arenas:
            #
            # OpenArena Error Messages
            # -
            openarena:
                #
                # Invalid Location Id
                # -
                # The "invalid-location-id" message is displayed to the player
                # if the player attempts to remove a location by an invalid
                # specified identifer.
                #
                invalid-location-id: "§cThe specified location id is invalid."

    #
    # Informative Messages
    # -
    informative:
        #
        # Informative Lobby Messages
        #
        lobby:
            #
            # Joined
            # -
            # The "joined" message is displayed to the player
            # once the player has joined the lobby. This is
            # only displayed via the command operation.
            #
            joined: "§6You have joined the lobby."

            #
            # Quit
            # -
            # The "quit" message is displayed to the player
            # once the player has quit the lobby. This is
            # only displayed via the command operation.
            #
            quit: "§6You have quit the lobby."

            #
            # Spawn Set
            # -
            # The "spawn-set" message is displayed to the player
            # once the spawn location for the lobby has been set.
            #
            spawn-set: "§6The lobby spawn location has been set to your current location."

            #
            # Region A Set
            # -
            # The "region-a-set" message is displayed to the player
            # once the player has specified the region A location via
            # the command editor.
            #
            region-a-set: "§6Region §fA §6value has been set successfully."

            #
            # Region B Set
            # -
            # The "region-b-set" message is displayed to the player
            # once the player has specified the region B location via
            # the command editor.
            #
            region-b-set: "§6Region §fB §6value has been set successfully."

            #
            # Region Ignore Y Set
            # -
            # The "region-ignore-y-set" message is displayed to the player
            # once the region ignore-y value has been set via the command editor.
            # This message has the following placeholders:
            #
            # {{VALUE}} - A boolean representation of the ignore-y value
            #
            region-ignore-y-set: "§6Region §fignore-y §6 has been set to §f{{VALUE}}§6."

        #
        # Informative Arena Messages
        # -
        arenas:
            #
            # Informative OpenArena Messages
            # -
            openarena:
                #
                # Name Set
                # -
                # The "name-set" message is displayed to the player once
                # the player has specified a name for the arena via the
                # command editor. This message has the following placeholders:
                #
                # {{ARENA_NAME}} - Displays the name of the arena
                #
                name-set: "§6The arena name has been set to §f{{ARENA_NAME}}§6."

                #
                # Display Name Set
                # -
                # The "display-name-set" message is displayed to the player
                # once the player has specified a display name for the arena
                # via the command editor. This message has the following
                # placeholders:
                #
                # {{ARENA_DISPLAY_NAME}} - Displays the arena display name
                #
                display-name-set: "§6The arena display name has been set to §f{{ARENA_DISPLAY_NAME}}§6."

                #
                # Spawn Set
                # -
                # The "spawn-set" message is displayed to the player
                # once the spawn location for the OpenArena has been set
                # via the command editor.
                #
                spawn-set: "§6The arena spawn location has been set to your current location."

                #
                # Location Added
                # -
                # The "location-added" message is displayed to the player
                # once a spawn location has been added to the OpenArena
                # via the command editor. This message has the following
                # placeholders:
                #
                # {{LOCATION_ID}} - Displays the newly created location's id
                #
                location-added: "§6Location §f#{{LOCATION_ID}} §6has been created."

                #
                # Location Removed
                # -
                # The "location-removed" message is displayed to the player
                # once the spawn location has been removed from the OpenArena
                # via the command editor. This message has the following
                # placeholders:
                #
                # {{LOCATION_ID}} - Displays the location's id that was removed
                #
                location-removed: "§6Location §f#{{LOCATION_ID}} §6has been removed."

                #
                # Location List Format
                # -
                # The "location-list-format" message is used to display the
                # list of locations in a specific format. This message has
                # the following placeholders:
                #
                # {{LOCATION_ID}} - Display's the location's id
                # {{LOCATION_X}} - Display's the location's X coordinate
                # {{LOCATION_Y}} - Display's the location's Y coordinate
                # {{LOCATION_Z}} - Display's the location's Z coordinate
                #
                location-list-format: "  §6{{LOCATION_ID}} - §f[ §bx: §7{{LOCATION_X}} §by: §7{{LOCATION_Y}} §bz: §7{{LOCATION_Z}} §f]"

                #
                # Location List Tooltip
                # -
                # The "location-list-tooltip" message is displayed above the
                # {{LOCATION_ID}} placeholder within the "location-list-format"
                # message.
                #
                location-list-tooltip: "§6Click here to teleport to the specified location."

                #
                # Region A Set
                # -
                # The "region-a-set" message is displayed to the player
                # once the player has specified the region A location via
                # the command editor.
                #
                region-a-set: "§6Region §fA §6value has been set successfully."

                #
                # Region B Set
                # -
                # The "region-b-set" message is displayed to the player
                # once the player has specified the region B location via
                # the command editor.
                #
                region-b-set: "§6Region §fB §6value has been set successfully."

                #
                # Region Ignore Y Set
                # -
                # The "region-ignore-y-set" message is displayed to the player
                # once the region ignore-y value has been set via the command editor.
                # This message has the following placeholders:
                #
                # {{VALUE}} - A boolean representation of the ignore-y value
                #
                region-ignore-y-set: "§6Region §fignore-y §6 has been set to §f{{VALUE}}§6."

            #
            # Informative CageArena Messages
            # -
            cagearena:
                #
                # Name Set
                # -
                # The "name-set" message is displayed to the player once
                # the player has specified a name for the arena via the
                # command editor. This message has the following placeholders:
                #
                # {{ARENA_NAME}} - Displays the name of the arena
                #
                name-set: "§6The arena name has been set to §f{{ARENA_NAME}}§6."

                #
                # Display Name Set
                # -
                # The "display-name-set" message is displayed to the player
                # once the player has specified a display name for the arena
                # via the command editor. This message has the following
                # placeholders:
                #
                # {{ARENA_DISPLAY_NAME}} - Displays the arena display name
                #
                display-name-set: "§6The arena display name has been set to §f{{ARENA_DISPLAY_NAME}}§6."

                #
                # Spawn Set
                # -
                # The "spawn-set" message is displayed to the player
                # once the spawn location for the OpenArena has been set
                # via the command editor.
                #
                spawn-set: "§6The arena spawn location has been set to your current location."

                #
                # Location A Set
                # -
                # The "location-a-set" message is displayed to the player
                # once the player has specified the location A location via
                # the command editor.
                #
                location-a-set: "§6The arena §7A §6spawn location has been set to your current location."

                #
                # Location B Set
                # -
                # The "location-b-set" message is displayed to the player
                # once the player has specified the location B location via
                # the command editor.
                #
                location-b-set: "§6The arena §7B §6spawn location has been set to your current location."

                #
                # Location List Format
                # -
                # The "location-list-format" message is used to display the
                # list of locations in a specific format. This message has
                # the following placeholders:
                #
                # {{LOCATION_ID}} - Display's the location's id
                # {{LOCATION_X}} - Display's the location's X coordinate
                # {{LOCATION_Y}} - Display's the location's Y coordinate
                # {{LOCATION_Z}} - Display's the location's Z coordinate
                #
                location-list-format: "  §6{{LOCATION_ID}} - §f[ §bx: §7{{LOCATION_X}} §by: §7{{LOCATION_Y}} §bz: §7{{LOCATION_Z}} §f]"

                #
                # Location List Tooltip
                # -
                # The "location-list-tooltip" message is displayed above the
                # {{LOCATION_ID}} placeholder within the "location-list-format"
                # message.
                #
                location-list-tooltip: "§6Click here to teleport to the specified location."

                #
                # Region A Set
                # -
                # The "region-a-set" message is displayed to the player
                # once the player has specified the region A location via
                # the command editor.
                #
                region-a-set: "§6Region §fA §6value has been set successfully."

                #
                # Region B Set
                # -
                # The "region-b-set" message is displayed to the player
                # once the player has specified the region B location via
                # the command editor.
                #
                region-b-set: "§6Region §fB §6value has been set successfully."

                #
                # Region Ignore Y Set
                # -
                # The "region-ignore-y-set" message is displayed to the player
                # once the region ignore-y value has been set via the command editor.
                # This message has the following placeholders:
                #
                # {{VALUE}} - A boolean representation of the ignore-y value
                #
                region-ignore-y-set: "§6Region §fignore-y §6 has been set to §f{{VALUE}}§6."

#
# Lobby
# -
lobby:
    #
    # Display Name
    # -
    # The "display-name" message is displayed to the player
    # once the player hovers over the lobby navigation item.
    #
    display-name: "§5[§dDracade§5]"

    #
    # Display Lore
    # -
    # The "display-lore" message is displayed to the player
    # once the player hovers over the lobby navigation item.
    # This message supports the newline character ("\n").
    #
    display-lore: >
                    §7Find a game by right-clicking the compass!\n

#
# Minigame
# -
minigames:
    #
    # Leave Item Name
    # -
    # The "leave-item-name" message is displayed to the player
    # once the player hovers over the minigame leave item.
    #
    leave-item-name: "§cRight click to leave."

    #
    # FreeForAll Minigame Messages
    #
    freeforall:
        #
        # Display Name
        # -
        # The "display-name" message is displayed to the player
        # on a variety of minigame related things such as the
        # scoreboard.
        #
        display-name: "§5[§dDracade§5]"

        #
        # You Killed
        # -
        # The "you-killed" message is displayed to the player
        # once they have killed another player within the minigame.
        # This message has the following placeholders:
        #
        # {{VICTIM_DISPLAY_NAME}} - Displays the display name of the victim
        #
        you-killed: "§dYou killed §7{{VICTIM_DISPLAY_NAME}}§d."

        #
        # Killed By
        # -
        # The "killed-by" message is displayed to the player
        # once they have been killed by another player within the minigame.
        # This message has the following placeholders:
        #
        # {{ATTACKER_DISPLAY_NAME}} - Displays the display name of the attacker
        # {{HEARTS}} - Displays the attacker's remaining hearts in decimal value
        #
        killed-by: "§dYou were killed by §7{{ATTACKER_DISPLAY_NAME}}§d, they had §7{{HEARTS}} §dhearts remaining."

        #
        # Scoreboard Messages
        # -
        # Note: The scoreboard messages cannot exceed 48 characters.
        # Exceeding the 48 character limit may cause issues.
        #
        scoreboard:
            #
            # Kills Message
            # -
            # The "kills-message" message is displayed to the player
            # above the "kills-score" message.
            #
            kills-message: "Your kills: "

            #
            # Kills Score
            # -
            # The "kills-score" message is displayed to the player
            # under the "kills-message" message. This message has
            # the following placeholders:
            #
            # {{KILLS}} - Displays the player's kill count
            #
            kills-score: "-> §d{{KILLS}}"

            #
            # Deaths Message
            # -
            # The "deaths-message" message is displayed to the player
            # above the "deaths-score" message.
            #
            deaths-message: "Your deaths: "

            #
            # Deaths Score
            # -
            # The "deaths-score" message is displayed to the player
            # under the "deaths-message" message. This message has
            # the following placeholders:
            #
            # {{DEATHS}} - Displays the player's death count
            #
            deaths-score: "-> §d{{DEATHS}}"

            #
            # Killstreak Message
            # -
            # The "killstreak-message" message is displayed to the player
            # above the "killstreak-score" message.
            #
            killstreak-message: "Killstreak: "

            #
            # Killstreak Score
            # -
            # The "killstreak-score" message is displayed to the player
            # under the "killstreak-message" message. This message has
            # the following placeholders:
            #
            # {{KILLSTREAK}} - Displays the player's current killstreak
            #
            killstreak-score: "-> §d{{KILLSTREAK}}"

            #
            # Ratio Message
            # -
            # The "ratio-message" message is displayed to the player
            # above the "ratio-score" message.
            #
            ratio-message: "K/D Ratio: "

            #
            # Ratio Score
            # -
            # The "ratio-score" message is displayed to the player
            # under the "ratio-message" message. This message has
            # the following placeholders:
            #
            # {{RATIO}} - Displays the player's Kills/Deaths ratio
            #
            ratio-score: "-> §d{{RATIO}}"

        #
        # Informative Menu Messages
        # -
        menu-info:
            #
            # Display Name
            # -
            # The "display-name" message is displayed to the player
            # once the player hovers over the freeforall gamemode option
            # in the navigation menu.
            #
            display-name: "§5[§dFreeForAll§5]"

            #
            # Display Lore
            # -
            # The "display-lore" message is displayed to the player
            # once the player hovers over the freeforall gamemode option
            # in the navigation menu. This message supports the newline
            # character ("\n").
            #
            display-lore: >
                            \n
                            §7FreeForAll allows you to battle\n
                            §7other players in an open arena.
    #
    # CageMatch Minigame Messages
    #
    cagematch:
        #
        # Display Name
        # -
        # The "display-name" message is displayed to the player
        # on a variety of minigame related things such as the
        # scoreboard.
        #
        display-name: "§5[§dDracade§5]"

        #
        # You Killed
        # -
        # The "you-killed" message is displayed to the player
        # once they have killed another player within the minigame.
        # This message has the following placeholders:
        #
        # {{VICTIM_DISPLAY_NAME}} - Displays the display name of the victim
        #
        you-killed: "§dYou killed §7{{VICTIM_DISPLAY_NAME}}§d."

        #
        # Killed By
        # -
        # The "killed-by" message is displayed to the player
        # once they have been killed by another player within the minigame.
        # This message has the following placeholders:
        #
        # {{ATTACKER_DISPLAY_NAME}} - Displays the display name of the attacker
        # {{HEARTS}} - Displays the attacker's remaining hearts in decimal value
        #
        killed-by: "§dYou were killed by §7{{ATTACKER_DISPLAY_NAME}}§d, they had §7{{HEARTS}} §dhearts remaining."

        #
        # You Won
        # -
        # The "you-won" message is displayed to the winning player.
        #
        you-won: "§dYou won the match!"

        #
        # You Lost
        # -
        # The "you-lost" message is displayed to the losing player.
        #
        you-lost: "§dYou lost the match!"

        #
        # You Drew
        # -
        # The "you-drew" message is displayed to the player if the
        # match result was a draw.
        #
        you-drew: "§dThe match was a draw!"

        #
        # Points Gained
        # -
        # The "points-gained" message is displayed to the winning player
        # displaying the amount of points they have gained. This message
        # has the following placeholders:
        #
        # {{POINTS}} - Displays the amount of points lost
        #
        points-gained: "§dYour rating increased by §6{{POINTS}} §dpoints."

        #
        # Points Lost
        # -
        # The "points-lost" message is displayed to the losing player
        # displaying the amount of points they have lost. This message
        # has the following placeholders:
        #
        # {{POINTS}} - Displays the amount of points gained
        #
        points-lost: "§dYour rating decreased by §6{{POINTS}} §dpoints."

        #
        # Scoreboard Messages
        # -
        # Note: The scoreboard messages cannot exceed 48 characters.
        # Exceeding the 48 character limit may cause issues.
        #
        scoreboard:
            #
            # Round Message
            # -
            #
            #
            round-message: "Round: "

            #
            # Round Score
            # -
            #
            #
            round-score: "-> §d{{CURRENT_ROUND}}§7/§d{{TOTAL_ROUNDS}}"

            #
            # Player A Message
            # -
            #
            #
            player-a-message: "{{PLAYER_DISPLAY_NAME}} Score: "

            #
            # Player A Score
            # -
            #
            #
            player-a-score: "-> §d{{SCORE}}"

            #
            # Player B Message
            # -
            #
            #
            player-b-message: "{{PLAYER_DISPLAY_NAME}} Score: "

            #
            # Player B Score
            # -
            #
            #
            player-b-score: "-> §d{{SCORE}}"

        #
        # Informative Menu Messages
        # -
        menu-info:
            #
            # Display Name
            # -
            # The "display-name" message is displayed to the player
            # once the player hovers over the freeforall gamemode option
            # in the navigation menu.
            #
            display-name: "§5[§dCageMatch§5]"

            #
            # Display Lore
            # -
            # The "display-lore" message is displayed to the player
            # once the player hovers over the cagematch gamemode option
            # in the navigation menu. This message supports the newline
            # character ("\n").
            #
            display-lore: >
                            \n
                            §7CageMatch allows you to battle\n
                            §7other players in competitive 1v1 matches.\n
                            \n§aJoin matchmaking queue.

            #
            # Display Queue Lore
            # -
            # The "display-queue-lore" message is displayed to the player
            # if the player exists within the queue, once the player hovers
            # over the cagematch gamemode option in the navigation menu.
            # This message supports the newline character ("\n").
            #
            display-queue-lore: >
                            \n
                            §7CageMatch allows you to battle\n
                            §7other players in competitive 1v1 matches.\n
                            \n§cLeave matchmaking queue.

            #
            # Added To Queue
            # -
            # The "added-to-queue" message is displayed to the player
            # once the player has joined the queue from the navigation
            # menu.
            #
            added-to-queue: "§dYou have joined the matchmaking queue... §7Please wait."

            #
            # Removed From Queue
            # -
            # The "removed-from-queue" message is displayed to the player
            # once the player has left the queue from the navigation
            # menu.
            #
            removed-from-queue: "§dYou have left the matchmaking queue."


#
# Minigame Menu
# -
minigame-menu:
    #
    # Inventory Title
    # -
    # The "inventory-title" message is displayed to the player
    # as the filter of which minigames are currently being
    # displayed. The filter may be returned as a specific minigame.
    # This message has the following placeholders:
    #
    # {{FILTER}} - Displays the minigame type
    #
    inventory-title: "§5{{FILTER}}"

    #
    # FreeForAll filter
    # -
    freeforall:
        #
        # Display Lore
        # -
        # The "display-lore" message is displayed to the player
        # once the player hovers over the freeforall minigame option
        # in the minigame menu. This message supports the newline
        # character ("\n") and has the following placeholders:
        #
        # {{PLAYERS}} - Displays the total amount of players currently playing the specified minigame
        # {{ARENA_DISPLAY_NAME}} - Displays the arena display name that is currently being played
        #
        display-lore: >
                        \n
                        §5{{PLAYERS}} §7players\n
                        §7currently playing on §5{{ARENA_DISPLAY_NAME}}

#
# Navigation Menu
# -
navigation-menu:
    #
    # Inventory Title
    # -
    # The "inventory-title" message is displayed to the player
    # as the name of the inventory menu.
    #
    inventory-title: "§5Navigation"

#
# Settings
# -
settings:
    #
    # Chat Messages
    # -
    chat:
        #
        # Format
        # -
        # The "format" message is used to display the chat
        # format to other players. This message has the following
        # placeholders:
        #
        # {{MMR}} - Display's the chat sender's MMR if the sender has one
        # {{PREFIX}} - Display's the chat sender's prefix if they have one
        # {{SUFFIX}} - Display's the chat sender's suffix if they have one
        # {{SENDER_NAME}} - Displays the chat sender's name
        # {{SENDER_DISPLAY_NAME}} - Displays the chat senders's display name
        # {{CHAT_MESSAGE}} - Display's the chat sender's message
        #
        format: "§6{{MMR}} §7| {{PREFIX}}{{SENDER_DISPLAY_NAME}}{{SUFFIX}} §f» §7{{CHAT_MESSAGE}}"


    #
    # Plugin Information Messages
    # -
    plugin-info:
        #
        # Name
        # -
        # The "name" message displays the plugin name. This
        # message has has the following placeholders:
        #
        # {{PLUGIN_NAME}} - Displays the plugin name
        #
        name: "§dname: §f{{PLUGIN_NAME}}"

        #
        # Version
        # -
        # The "version" message displays the plugin version.
        # This message has the following placeholders:
        #
        # {{PLUGIN_VERSION}} - Displays the plugin version
        #
        version: "§dversion: §f{{PLUGIN_VERSION}}"

        #
        # Description
        # -
        # The "description" message displays the plugin description.
        # This message has the following placeholders:
        #
        description: "§ddescription: §fA collection of competitive combat based minigames."

        #
        # Developer
        # -
        # The "developer" message displays the plugin developer.
        # This message has the following placeholders:
        #
        # {{DEVELOPER}} - Displays the developer's name
        #
        developer: "§ddeveloper: §f{{DEVELOPER}}"

    #
    # Arena Information Messages
    # -
    arena-info:
        #
        # OpenArena Information Messages
        # -
        openarena:
            #
            # Name
            # -
            # The "name" message displays the name of the currently
            # selected arena within the command editor. This message
            # has the following placeholders:
            #
            # {{ARENA_NAME}} - Displays the name of the arena
            #
            name: "§dName: §f{{ARENA_NAME}}"

            #
            # Display Name
            # -
            # The "display-name" message displays the arena's display
            # name of the currently selected arena within the command
            # editor. This message has the following placeholders:
            #
            # {{ARENA_DISPLAY_NAME}} - Displays the arena display name
            #
            display-name: "§dDisplay name: §f{{ARENA_DISPLAY_NAME}}"