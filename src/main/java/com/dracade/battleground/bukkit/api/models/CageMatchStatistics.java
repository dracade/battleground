package com.dracade.battleground.bukkit.api.models;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.validation.NotNull;
import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.utilities.MatchmakingRating;
import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Optional;
import java.util.UUID;

@Entity()
@Table(name = "battleground_cagematch_statistics")
public class CageMatchStatistics implements MatchmakingRating {

    @Id
    private int id;

    @NotNull
    @Column(name = "uniqueId", unique = true)
    private UUID uniqueId;

    @NotNull
    @Column(name = "rating")
    private double rating;

    @NotNull
    @Column(name = "rankedKills")
    private int rankedKills;

    @NotNull
    @Column(name = "rankedDeaths")
    private int rankedDeaths;

    @NotNull
    @Column(name = "rankedWins")
    private int rankedWins;

    @NotNull
    @Column(name = "rankedLoses")
    private int rankedLoses;

    @NotNull
    @Column(name = "rankedDraws")
    private int rankedDraws;

    @NotNull
    @Column(name = "unrankedKills")
    private int unrankedKills;

    @NotNull
    @Column(name = "unrankedDeaths")
    private int unrankedDeaths;

    @NotNull
    @Column(name = "unrankedWins")
    private int unrankedWins;

    @NotNull
    @Column(name = "unrankedLoses")
    private int unrankedLoses;

    @NotNull
    @Column(name = "unrankedDraws")
    private int unrankedDraws;

    @NotNull
    @Column(name = "totalTimePlayed")
    private long totalTimePlayed;

    private transient int kills;
    private transient int deaths;

    private transient boolean isNewRecord;

    /**
     * Create a new CageMatchStatistics instance.
     */
    public CageMatchStatistics() {
        this.rating = 1000;
        this.kills = 0;
        this.deaths = 0;
        this.isNewRecord = false;
    }

    /**
     * Set the id of the record.
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retrieve the record id.
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     * Set the player's id.
     *
     * @param uniqueId
     */
    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Retrieve the player's id.
     *
     * @return
     */
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    @Override
    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public double getRating() {
        return this.rating;
    }

    /**
     * Set whether or not the daya is apart of a new record.
     *
     * @param isNewRecord
     */
    public void setNewRecord(boolean isNewRecord) {
        this.isNewRecord = isNewRecord;
    }

    /**
     * Determine if the data is apart of a new record.
     *
     * @return
     */
    public boolean isNewRecord() {
        return this.isNewRecord;
    }

    /**
     * Set the amount of kills the player has had in ranked games.
     *
     * @param rankedKills The amount of kills
     * @return The current amount of kills
     */
    public void setRankedKills(int rankedKills) {
        this.rankedKills = rankedKills;
    }

    /**
     * Retrieve the amount of ranked kills.
     *
     * @return The amount of kills achieved
     */
    public int getRankedKills() {
        return this.rankedKills;
    }

    /**
     * Set the amount of deaths the player has had in ranked games.
     *
     * @param rankedDeaths The amount of deaths
     * @return The current amount of deaths
     */
    public void setRankedDeaths(int rankedDeaths) {
        this.rankedDeaths = rankedDeaths;
    }

    /**
     * Retrieve the amount of ranked deaths.
     *
     * @return The amount of deaths received
     */
    public int getRankedDeaths() {
        return this.rankedDeaths;
    }

    /**
     * Set the total amount of ranked wins.
     *
     * @param rankedWins
     */
    public void setRankedWins(int rankedWins) {
        this.rankedWins = rankedWins;
    }

    /**
     * Get the total amount of ranked wins.
     *
     * @return
     */
    public int getRankedWins() {
        return this.rankedWins;
    }

    /**
     * Set the total amount of ranked loses.
     *
     * @param rankedLoses
     */
    public void setRankedLoses(int rankedLoses) {
        this.rankedLoses = rankedLoses;
    }

    /**
     * Get the total amount of ranked loses.
     *
     * @return
     */
    public int getRankedLoses() {
        return this.rankedLoses;
    }

    /**
     * Set the total amount of ranked draws.
     *
     * @param rankedDraws
     */
    public void setRankedDraws(int rankedDraws) {
        this.rankedDraws = rankedDraws;
    }

    /**
     * Get the total amount of ranked draws.
     *
     * @return
     */
    public int getRankedDraws() {
        return this.rankedDraws;
    }

    /**
     * Set the amount of kills the player has had in unranked games.
     *
     * @param unrankedKills The amount of kills
     * @return The current amount of kills
     */
    public void setUnrankedKills(int unrankedKills) {
        this.unrankedKills = unrankedKills;
    }

    /**
     * Retrieve the amount of unranked kills.
     *
     * @return The amount of kills achieved
     */
    public int getUnrankedKills() {
        return this.unrankedKills;
    }

    /**
     * Set the amount of deaths the player has had in unranked games.
     *
     * @param unrankedDeaths The amount of deaths
     * @return The current amount of deaths
     */
    public void setUnrankedDeaths(int unrankedDeaths) {
        this.unrankedDeaths = unrankedDeaths;
    }

    /**
     * Retrieve the amount of unranked deaths.
     *
     * @return The amount of deaths received
     */
    public int getUnrankedDeaths() {
        return this.unrankedDeaths;
    }

    /**
     * Set the total amount of unranked wins.
     *
     * @param unrankedWins
     */
    public void setUnrankedWins(int unrankedWins) {
        this.unrankedWins = unrankedWins;
    }

    /**
     * Get the total amount of unranked wins.
     *
     * @return
     */
    public int getUnrankedWins() {
        return this.unrankedWins;
    }

    /**
     * Set the total amount of unranked loses.
     *
     * @param unrankedLoses
     */
    public void setUnrankedLoses(int unrankedLoses) {
        this.unrankedLoses = unrankedLoses;
    }

    /**
     * Get the total amount of unranked loses.
     *
     * @return
     */
    public int getUnrankedLoses() {
        return this.unrankedLoses;
    }

    /**
     * Set the total amount of unranked draws.
     *
     * @param unrankedDraws
     */
    public void setUnrankedDraws(int unrankedDraws) {
        this.unrankedDraws = unrankedDraws;
    }

    /**
     * Get the total amount of unranked draws.
     *
     * @return
     */
    public int getUnrankedDraws() {
        return this.unrankedDraws;
    }

    /**
     * Set the amount of kills the player has had.
     *
     * @param kills The amount of kills
     * @return The current amount of kills
     */
    public void setKills(int kills) {
        this.kills = kills;
    }

    /**
     * Retrieve the amount of kills.
     *
     * @return The amount of kills achieved
     */
    public int getKills() {
        return this.kills;
    }

    /**
     * Set the amount of deaths the player has had.
     *
     * @param deaths The amount of deaths
     * @return The current amount of deaths
     */
    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    /**
     * Retrieve the amount of deaths.
     *
     * @return The amount of deaths received
     */
    public int getDeaths() {
        return this.deaths;
    }

    /**
     * Set the total play time.
     *
     * @param totalTimePlayed
     */
    public void setTotalTimePlayed(long totalTimePlayed) {
        this.totalTimePlayed = totalTimePlayed;
    }

    /**
     * Retrieve the total amount of time played.
     *
     * @return
     */
    public long getTotalTimePlayed() {
        return this.totalTimePlayed;
    }

    /**
     * Data Access Object
     */
    public static class DAO {

        private static Cache<UUID, CageMatchStatistics> cache = CacheBuilder.newBuilder()
                .weakKeys()
                .build();

        /**
         * Check if the value is cached.
         *
         * @param uniqueId
         * @return
         */
        public static boolean isCached(UUID uniqueId) {
            return DAO.cache.getIfPresent(uniqueId) != null;
        }

        /**
         * Get or create the statistics for the specified player.
         *
         * @param uniqueId The player's unique id
         * @return
         */
        public static CageMatchStatistics getOrCreate(UUID uniqueId) {
            Optional<EbeanServer> database = BattlegroundBukkit.plugin().getDatabaseHandler().getDatabase();
            CageMatchStatistics statistics = DAO.cache.getIfPresent(uniqueId);

            if (statistics == null) {
                // if a database connection exists
                if (database.isPresent()) {
                    statistics = database.get().find(CageMatchStatistics.class).where().eq("uniqueId", uniqueId).findUnique();
                }

                // if the player's statistics weren't found
                if (statistics == null) {
                    statistics = new CageMatchStatistics();

                    statistics.setUniqueId(uniqueId);
                    statistics.setRating(1000);
                    statistics.setNewRecord(true);
                }

                DAO.cache.put(uniqueId, statistics);
            }

            return statistics;
        }

        /**
         * Save the specified statistics instance.
         *
         * @param statistics
         */
        public static void save(CageMatchStatistics statistics) {
            Preconditions.checkNotNull(statistics);

            Optional<EbeanServer> database = BattlegroundBukkit.plugin().getDatabaseHandler().getDatabase();
            if (database.isPresent()) {
                if (statistics.isNewRecord()) {
                    database.get().save(statistics);
                } else {
                    database.get().update(statistics);
                }
            }

            DAO.cache.put(statistics.getUniqueId(), statistics);
        }

    }

}
