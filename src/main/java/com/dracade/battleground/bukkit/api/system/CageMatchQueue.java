package com.dracade.battleground.bukkit.api.system;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.arenas.CageArena;
import com.dracade.battleground.bukkit.api.minigames.CageMatch;
import com.dracade.battleground.bukkit.api.models.CageMatchStatistics;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.events.queue.QueueRemoveEvent;
import com.dracade.vone.bukkit.api.exceptions.ArenaActiveException;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.system.VoneQueue;
import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class CageMatchQueue extends VoneQueue<CageMatchQueueItem> implements Listener, Runnable {

    private static CageMatchQueue instance;

    /**
     * Retrieve the CageMatchQueue instance.
     *
     * @return
     */
    public static CageMatchQueue getInstance() {
        return (CageMatchQueue.instance != null) ? CageMatchQueue.instance : (CageMatchQueue.instance = new CageMatchQueue());
    }

    /**
     * Create a new CageMatchQueue.
     */
    private CageMatchQueue() {
        Bukkit.getPluginManager().registerEvents(this, BattlegroundBukkit.plugin());
        Bukkit.getScheduler().runTaskTimer(BattlegroundBukkit.plugin(), this, 0L, 20L * 5);
    }

    @Override
    public boolean remove(Object o) {
        Preconditions.checkNotNull(o);
        if (o instanceof UUID) {
            for (Iterator<CageMatchQueueItem> iter = this.iterator(); iter.hasNext();) {
                CageMatchQueueItem i = iter.next();
                if (i.getQueueHolder().equals(o) || (i.getOpponent().isPresent() ? i.getOpponent().get().equals(o) : false)) {
                    return super.remove(i);
                }
            }
        }
        return super.contains(o);
    }

    @Override
    public boolean contains(Object o) {
        Preconditions.checkNotNull(o);
        if (o instanceof UUID) {
            for (CageMatchQueueItem i : this) {
                if (i.getQueueHolder().equals(o) || (i.getOpponent().isPresent() ? i.getOpponent().get().equals(o) : false)) {
                    return true;
                }
            }
        }
        return super.contains(o);
    }

    /**
     * Check if a game can be played for the next position
     * in the queue.
     */
    @Override
    public void run() {
        List<Arena> inactive = VoneBukkit.plugin().registry().getInactiveArenas().stream().filter(arena -> (arena instanceof CageArena)).collect(Collectors.toList());
        if (inactive.size() > 0) {
            CageMatchQueueItem firstQueueItem = this.peek();

            // If the queue isn't empty, then ...
            if (firstQueueItem != null) {
                Player queuedPlayer = Bukkit.getPlayer(firstQueueItem.getQueueHolder());
                if (queuedPlayer != null) {
                    Player matchedPlayer = null;
                    CageMatchQueueItem matchedQueueItem = null;

                    if (firstQueueItem.getOpponent().isPresent()) {
                        matchedPlayer = Bukkit.getPlayer(firstQueueItem.getOpponent().get());
                    }

                    // If the player did not have an opponent, then
                    // we search for a player with a similar Matchmaking Rating
                    if (matchedPlayer == null) {
                        double rating = CageMatchStatistics.DAO.getOrCreate(firstQueueItem.getQueueHolder()).getRating();
                        double range = (rating * 0.1);

                        check:
                        while (matchedPlayer == null) {
                            search:
                            for (CageMatchQueueItem i : this) {
                                if (!i.getQueueHolder().equals(firstQueueItem.getQueueHolder())) {
                                    double iRating = CageMatchStatistics.DAO.getOrCreate(i.getQueueHolder()).getRating();

                                    if ((iRating - range < rating) || (rating + range > iRating)) {
                                        matchedPlayer = Bukkit.getPlayer(i.getQueueHolder());
                                        matchedQueueItem = i;
                                        break search;
                                    }
                                }
                            }

                            range = range * 2;
                            if (rating / 2 > range) {
                                break check;
                            }
                        }
                    }

                    // Attempt to create the match
                    if (matchedPlayer != null) {
                        try {
                            this.remove(queuedPlayer.getUniqueId());
                            this.remove(matchedPlayer.getUniqueId());

                            VoneBukkit.plugin().registry().register(new CageMatch((CageArena) inactive.get(0), 3, queuedPlayer, matchedPlayer, true));

                            // send to each player their position in the queue
                        } catch (ArenaActiveException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

}
