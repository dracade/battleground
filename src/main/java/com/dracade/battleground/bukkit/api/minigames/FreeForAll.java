package com.dracade.battleground.bukkit.api.minigames;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.arenas.OpenArena;
import com.dracade.battleground.bukkit.api.kits.BattlegroundKit;
import com.dracade.battleground.bukkit.api.kits.IronKit;
import com.dracade.battleground.bukkit.api.models.FreeForAllStatistics;
import com.dracade.battleground.bukkit.api.system.CageMatchQueue;
import com.dracade.battleground.bukkit.api.utilities.CombatLog;
import com.dracade.vone.bukkit.api.events.minigame.MinigameJoinEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameLoadedEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameQuitEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameStartedEvent;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.game.Playable;
import com.dracade.vone.bukkit.api.utilities.FancyScoreboard;
import com.dracade.vone.bukkit.VoneBukkit;
import com.google.common.collect.ImmutableList;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.*;
import org.bukkit.util.Vector;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.*;

public class FreeForAll implements Minigame, Playable {

    private OpenArena arena;
    private Map<UUID, DataContainer> players;
    private UUID uniqueId;

    private Set<ItemStack> apples;

    private ItemStack minigameLeaveItem;

    /**
     * Create a new FreeForAll instance.
     *
     * @param arena The arena you wish to play on
     */
    public FreeForAll(OpenArena arena) {
        this.arena = arena;
        this.players = new HashMap<UUID, DataContainer>();
        this.uniqueId = UUID.randomUUID();

        this.apples = new HashSet<ItemStack>();

        this.minigameLeaveItem = new ItemStack(Material.REDSTONE_BLOCK);

        Bukkit.getPluginManager().callEvent(new MinigameLoadedEvent(this));
    }

    @Override
    public long getDelay() {
        return 0;
    }

    @Override
    public long getInterval() {
        return 0;
    }

    @Override
    public OpenArena getArena() {
        return this.arena;
    }

    @Override
    public boolean addPlayer(UUID uniqueId) {
        boolean add = false;
        if (!this.players.containsKey(uniqueId)) {
            Player player = Bukkit.getPlayer(uniqueId);

            if (player != null) {
                MinigameJoinEvent event = new MinigameJoinEvent(this, player);
                Bukkit.getPluginManager().callEvent(event);

                event.setCancelled(CageMatchQueue.getInstance().stream().anyMatch(a -> a.getQueueHolder().equals(uniqueId)));

                if (!event.isCancelled()) {
                    Locale locale = VoneBukkit.plugin().getPlayerLocale(player);
                    String scoreboardName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.display-name");

                    FancyScoreboard finalScoreboard = new FancyScoreboard(scoreboardName);

                    add = true;

                    /**
                     * Run queries asynchronously to prevent the holding
                     * up of the main thread. (Bukkit issues)
                     */
                    Bukkit.getScheduler().runTaskAsynchronously(BattlegroundBukkit.plugin(), () -> {
                        final FreeForAllStatistics finalStatistics = FreeForAllStatistics.DAO.getOrCreate(player.getUniqueId());

                        /**
                         * TODO: Load the player's kit
                         */
                        final BattlegroundKit finalKit = new IronKit(player.getUniqueId())
                                .setMinigameLeaveItem(this.minigameLeaveItem);

                        /**
                         * Run Bukkit-related tasks synchronously (Bukkit issues)
                         */
                        Bukkit.getScheduler().runTask(BattlegroundBukkit.plugin(), () -> {
                            // Register the kit as an event listener
                            Bukkit.getPluginManager().registerEvents(finalKit, BattlegroundBukkit.plugin());

                            this.players.put(player.getUniqueId(), new DataContainer(finalStatistics, finalScoreboard, finalKit));

                            this.clean(player.getUniqueId());
                            this.spawn(player.getUniqueId());
                            this.init(player.getUniqueId());
                        });
                    });
                }
            }
        }
        return add;
    }

    @Override
    public boolean removePlayer(UUID uniqueId) {
        boolean remove = false;
        if (this.players.containsKey(uniqueId)) {
            Player player = Bukkit.getPlayer(uniqueId);

            if (player != null && !BattlegroundBukkit.plugin().isServerStopping()) {
                MinigameQuitEvent event = new MinigameQuitEvent(this, player);
                Bukkit.getPluginManager().callEvent(event);

                remove = !event.isCancelled();
            }

            if (remove || BattlegroundBukkit.plugin().isServerStopping()) {
                // Remove player
                DataContainer data = this.players.remove(uniqueId);

                if (data != null) {
                    // Remove scoreboards
                    Optional<FancyScoreboard> scoreboard = data.getScoreboard();
                    if (scoreboard.isPresent()) {
                        scoreboard.get().reset(true);
                    }

                    // Unregister the kit event listener
                    Optional<BattlegroundKit> kit = data.getKit();
                    if (kit.isPresent()) {
                        HandlerList.unregisterAll(kit.get());
                    }
                }
                remove = true;
            }
        }
        return remove;
    }

    @Override
    public boolean containsPlayer(UUID uniqueId) {
        return this.players.containsKey(uniqueId);
    }

    @Override
    public ImmutableList<UUID> getPlayers() {
        return ImmutableList.copyOf(this.players.keySet());
    }

    @Override
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    @Override
    public void run() {
        /**
         * Removed forever updating scoreboard ;)
         */
    }

    /**
     * DataContainer to hold all of a player's data.
     */
    private class DataContainer {

        private long creationTime;
        private FreeForAllStatistics statistics;
        private FancyScoreboard scoreboard;
        private BattlegroundKit kit;
        private boolean godmode;

        /**
         * Create a new DataContainer instance.
         *
         * @param statistics The statisitcs to hold
         * @param scoreboard The scoreboard to hold
         * @param kit The kit to hold
         */
        public DataContainer(FreeForAllStatistics statistics, FancyScoreboard scoreboard, BattlegroundKit kit) {
            this.creationTime = System.currentTimeMillis();
            this.statistics = statistics;
            this.scoreboard = scoreboard;
            this.kit = kit;
            this.godmode = false;
        }

        /**
         * Set the player's god mode.
         *
         * @param godmode
         */
        public void setGodMode(boolean godmode) {
            this.godmode = godmode;
        }

        /**
         * Check if the player's immune.
         *
         * @return
         */
        public boolean isGodMode() {
            return this.godmode;
        }

        /**
         * Set the container to hold the specified statistics.
         *
         * @param statistics
         */
        public void setStatistics(FreeForAllStatistics statistics) {
            this.statistics = statistics;
        }

        /**
         * Retrieve the held statistics.
         *
         * @return
         */
        public Optional<FreeForAllStatistics> getStatistics() {
            return Optional.ofNullable(this.statistics);
        }

        /**
         * Save the specified player's statistics.
         */
        public void saveStatistics() {
            if (this.statistics != null) {
                this.statistics.setTotalKills(this.statistics.getTotalKills() + this.statistics.getKills());
                this.statistics.setTotalDeaths(this.statistics.getTotalDeaths() + this.statistics.getDeaths());
                this.statistics.setTotalTimePlayed(this.statistics.getTotalTimePlayed() + (System.currentTimeMillis() - this.creationTime));

                if (BattlegroundBukkit.plugin().isServerStopping()) {
                    FreeForAllStatistics.DAO.save(this.statistics);
                } else {
                    Bukkit.getScheduler().runTaskAsynchronously(BattlegroundBukkit.plugin(), () -> {
                        FreeForAllStatistics.DAO.save(this.statistics);
                    });
                }
            }
        }

        /**
         * Set the container to hold the specified scoreboard.
         *
         * @param scoreboard
         */
        public void setScoreboard(FancyScoreboard scoreboard) {
            this.scoreboard = scoreboard;
        }

        /**
         * Retrieve the held scoreboard.
         *
         * @return
         */
        public Optional<FancyScoreboard> getScoreboard() {
            return Optional.ofNullable(this.scoreboard);
        }

        /**
         * Set the container to hold the specified kit.
         *
         * @param kit
         */
        public void setKit(BattlegroundKit kit) {
            this.kit = kit;
        }

        /**
         * Retrieve the held kit.
         *
         * @return
         */
        public Optional<BattlegroundKit> getKit() {
            return Optional.ofNullable(this.kit);
        }

    }

    /**
     * Set the data container of the player.
     *
     * @param uniqueId the id of the player
     * @param container the player's statistics
     */
    public void setDataContainer(UUID uniqueId, DataContainer container) {
        if (this.players.containsKey(uniqueId)) {
            this.players.put(uniqueId, container);
        }
    }

    /**
     * Retrieve the statistics for a specified player.
     *
     * @param uniqueId The id of the player
     * @return An empty optional wrapper if the player's statistics could not be found
     */
    public Optional<DataContainer> getDataContainer(UUID uniqueId) {
        return Optional.ofNullable(this.players.get(uniqueId));
    }

    /**
     * Clean the player ready to spawn.
     *
     * @param uniqueId The id of the player you wish to clean
     */
    private void clean(UUID uniqueId) {
        Player player = Bukkit.getPlayer(uniqueId);
        if (player != null) {
            player.setGameMode(GameMode.SURVIVAL);
            player.setHealth(20);
            player.setFoodLevel(20);
            player.setExp(0);
            player.setLevel(0);
            player.setFallDistance(0);
            player.setFireTicks(0);
        }
    }

    /**
     * Spawn the player.
     *
     * @param uniqueId The id of the player you wish to spawn
     */
    private void spawn(UUID uniqueId) {
        Player player = Bukkit.getPlayer(uniqueId);
        if (player != null) {
            player.setFallDistance(0);
            player.teleport(this.arena.getRandomLocation());
        }
    }

    /**
     * Init the player.
     *
     * @param uniqueId The id of the player you wish to init
     */
    private void init(UUID uniqueId) {
        Player player = Bukkit.getPlayer(uniqueId);
        if (player != null) {
            Optional<DataContainer> data = this.getDataContainer(player.getUniqueId());
            if (data.isPresent()) {
                data.get().setGodMode(true);

                Optional<BattlegroundKit> kit = data.get().getKit();
                if (kit.isPresent()) {
                    kit.get().give(player);
                }
            }
            this.updateScoreboard(player);
        }
    }

    /**
     * Update the player's scoreboard.
     *
     * @param player
     */
    private void updateScoreboard(Player player) {
        Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

        Optional<DataContainer> data = this.getDataContainer(player.getUniqueId());
        if (data.isPresent()) {
            Optional<FreeForAllStatistics> statistics = data.get().getStatistics();
            if (statistics.isPresent()) {
                Optional<FancyScoreboard> scoreboard = data.get().getScoreboard();
                if (scoreboard.isPresent()) {
                    scoreboard.get()
                            .reset()
                            .blankLine()
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.kills-message"))
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.kills-score").replace("{{KILLS}}", String.valueOf(statistics.get().getKills())))
                            .blankLine()
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.deaths-message"))
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.deaths-score").replace("{{DEATHS}}", String.valueOf(statistics.get().getDeaths())))
                            .blankLine()
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.killstreak-message"))
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.killstreak-score").replace("{{KILLSTREAK}}", String.valueOf(statistics.get().getKillstreak())))
                            .blankLine()
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.ratio-message"))
                            .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.scoreboard.ratio-score").replace("{{RATIO}}", String.format("%.2f", statistics.get().currentRatio())))
                            .blankLine()
                            .build()
                            .send(player);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onMinigameStarted(MinigameStartedEvent event) {
        if (event.getMinigame().equals(this)) {
            this.arena.getRegion().removeEntities(Item.class, Monster.class, Animals.class);

            this.getPlayers().stream().forEach(id -> {
                Player player = Bukkit.getPlayer(id);
                if (player != null) {
                    this.clean(player.getUniqueId());
                    this.spawn(player.getUniqueId());
                    this.init(player.getUniqueId());
                } else {
                    this.players.remove(id);
                }
            });
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onMinigameQuit(MinigameQuitEvent event) {
        if (event.getMinigame().equals(this)) {
            Optional<DataContainer> data = this.getDataContainer(event.getPlayer().getUniqueId());
            if (data.isPresent()) {
                data.get().saveStatistics();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerDeath(PlayerDeathEvent event) {
        event.setDeathMessage("");

        if (this.containsPlayer(event.getEntity().getUniqueId())) {
            Player victim = event.getEntity();

            // Golden apple drop
            ItemStack drop = new ItemStack(Material.GOLDEN_APPLE, 1);

            this.apples.add(drop);

            event.setDroppedExp(0);
            event.getDrops().clear();
            event.getDrops().add(drop);

            // force respawn after 3s
            Bukkit.getScheduler().scheduleSyncDelayedTask(VoneBukkit.plugin(), () -> {
                victim.spigot().respawn();
            }, 20L * 3);

            if (event.getEntity().getKiller() != null) {
                this.simulateDeath(event.getEntity().getUniqueId(), event.getEntity().getKiller().getUniqueId());
            } else {
                this.simulateDeath(event.getEntity().getUniqueId());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerRespawn(PlayerRespawnEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            event.setRespawnLocation(this.arena.getRandomLocation());
            Bukkit.getScheduler().runTaskLater(VoneBukkit.plugin(), () -> {
                this.clean(event.getPlayer().getUniqueId());
                this.init(event.getPlayer().getUniqueId());
            }, 10L);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onItemSpawn(ItemSpawnEvent event) {
        if (this.apples.contains(event.getEntity().getItemStack())) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(BattlegroundBukkit.plugin(), () -> {
                event.getEntity().remove();
            }, 20L * 7);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onItemPickup(PlayerPickupItemEvent event) {
        if (this.apples.contains(event.getItem().getItemStack())) {
            event.setCancelled(true);

            if (this.containsPlayer(event.getPlayer().getUniqueId())) {
                event.getItem().remove();
                event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 7, 2));
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            if (this.containsPlayer(((Player) event.getEntity()).getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockIgnite(BlockIgniteEvent event) {
        if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
            if (event.getCause() == BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(BattlegroundBukkit.plugin(), () -> {
                    event.getBlock().setType(Material.AIR);
                }, 20L * 7);
            } else {
                if (event.getPlayer() != null) {
                    if (this.containsPlayer(event.getPlayer().getUniqueId())) {
                        event.setCancelled(true);
                    }
                } else {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBurn(BlockBurnEvent event) {
        if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockPlace(BlockPlaceEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (!event.getBlock().getType().equals(Material.FIRE)) {
                if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBreak(BlockBreakEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerMove(PlayerMoveEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (!event.getFrom().getBlock().getLocation().equals(event.getTo().getBlock().getLocation())) {
                Optional<DataContainer> data = this.getDataContainer(event.getPlayer().getUniqueId());
                if (data.isPresent()) {
                    if (data.get().isGodMode()) {
                        data.get().setGodMode(false);
                    }
                }
            }

            if (!this.getArena().getRegion().contains(event.getTo())) {
                Location loc = event.getPlayer().getLocation();
                if (loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ()).getType().equals(Material.AIR)) {
                    if (!event.getPlayer().isDead()) {
                        event.getPlayer().setHealth(0);
                    }
                } else {
                    event.getPlayer().teleport(event.getFrom());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerQuit(PlayerQuitEvent event) {
        if (CombatLog.getInstance().check(event.getPlayer().getUniqueId())) {
            Optional<UUID> lastAttacker = CombatLog.getInstance().getLastAttackingPlayer(event.getPlayer().getUniqueId());
            if (lastAttacker.isPresent()) {
                this.simulateDeath(event.getPlayer().getUniqueId(), lastAttacker.get());
            }
        }
        this.removePlayer(event.getPlayer().getUniqueId());
    }

    /**
     * Simulate a death/kill.
     *
     * @param victimId
     */
    private void simulateDeath(UUID victimId) {
        this.simulateDeath(victimId, null);
    }

    /**
     * Simulate a death/kill.
     *
     * @param victimId
     * @param attackerId
     */
    private void simulateDeath(UUID victimId, UUID attackerId) {
        Optional<DataContainer> victimData = this.getDataContainer(victimId);

        if (victimData.isPresent()) {
            Optional<FreeForAllStatistics> victimStats = victimData.get().getStatistics();

            if (victimStats.isPresent()) {
                victimStats.get().setKillstreak(0);
                victimStats.get().setDeaths(victimStats.get().getDeaths() + 1);
            }
        }

        Player victim = Bukkit.getPlayer(victimId);
        Player attacker = Bukkit.getPlayer(attackerId);

        if (attacker != null && this.containsPlayer(attacker.getUniqueId())) {
            if (victim != null) {
                Locale victimLocale = VoneBukkit.plugin().getPlayerLocale(victim);
                double attackerHealth = (attacker.getHealth() > 0) ? (attacker.getHealth() / 2) : 0;
                String victimMessage = BattlegroundBukkit.plugin().i18n().translateDirect(victimLocale, "minigames.freeforall.killed-by")
                        .replace("{{ATTACKER_DISPLAY_NAME}}", attacker.getDisplayName())
                        .replace("{{HEARTS}}", String.format("%.2f", attackerHealth));

                victim.sendMessage(victimMessage);
            }

            Locale attackerLocale = VoneBukkit.plugin().getPlayerLocale(attacker);
            String attackerMessage = BattlegroundBukkit.plugin().i18n().translateDirect(attackerLocale, "minigames.freeforall.you-killed")
                    .replace("{{VICTIM_DISPLAY_NAME}}", victim.getDisplayName());

            attacker.sendMessage(attackerMessage);

            Optional<DataContainer> attackerData = this.getDataContainer(attacker.getUniqueId());

            if (attackerData.isPresent()) {
                Optional<FreeForAllStatistics> attackerStats = attackerData.get().getStatistics();

                if (attackerStats.isPresent()) {
                    attackerStats.get().setKills(attackerStats.get().getKills() + 1);
                    attackerStats.get().setKillstreak(attackerStats.get().getKillstreak() + 1);
                }
            }

            this.updateScoreboard(attacker);
            CombatLog.getInstance().remove(attackerId);
        }

        this.updateScoreboard(victim);
        CombatLog.getInstance().remove(victimId);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerInteract(PlayerInteractEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                if (event.getPlayer().getInventory().getItemInHand().equals(this.minigameLeaveItem)) {
                    if (CombatLog.getInstance().check(event.getPlayer().getUniqueId())) {
                        String message = BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(event.getPlayer()), "messages.informative.combat-log.you-are-logged");
                        event.getPlayer().sendMessage(message);
                    } else {
                        this.removePlayer(event.getPlayer().getUniqueId());
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (event.getRightClicked().getType().equals(EntityType.ITEM_FRAME)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Player damager = null;
        boolean hitByProjectile = false;

        /**
         * If the damager is an instance of player
         */
        if (event.getDamager() instanceof Player) {
            damager = (Player) event.getDamager();
            if (damager != null) {
                CombatLog.getInstance().update(event.getEntity().getUniqueId(), event.getDamager().getUniqueId());
            }
        }

        else if (event.getDamager() instanceof Projectile) {
            Projectile projectile = (Projectile) event.getDamager();
            hitByProjectile = true;
            if (projectile.getShooter() instanceof Player) {
                damager = (Player) projectile.getShooter();
            }
        }

        /**
         * If the victim entity is an ItemFrame
         */
        if (event.getEntity() instanceof ItemFrame) {
            if (damager != null) {
                if (this.containsPlayer(damager.getUniqueId())) {
                    event.setCancelled(true);
                }
            }
        }

        /**
         * If the victim entity is a Player
         */
        else if (event.getEntity() instanceof Player) {
            Player victim = (Player) event.getEntity();
            Optional<DataContainer> data = this.getDataContainer(victim.getUniqueId());

            if (data.isPresent()) {
                if (data.get().isGodMode()) {
                    event.setCancelled(true);
                }
            } else {
                if (this.containsPlayer(victim.getUniqueId())) {
                    if (damager != null && hitByProjectile) {
                        damager.playSound(damager.getLocation(), Sound.ORB_PICKUP, 1, 0);
                    }
                }
            }
        }

        if (event.getEntity() instanceof Player && this.containsPlayer(event.getEntity().getUniqueId())) {
            if (damager != null && this.containsPlayer(damager.getUniqueId())) {
                CombatLog.getInstance().update(event.getEntity().getUniqueId(), event.getDamager().getUniqueId());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onInventoryClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            if (this.containsPlayer(player.getUniqueId())) {
                if (event.getCurrentItem() != null) {
                    if (event.getCurrentItem().equals(this.minigameLeaveItem)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
        if (event.getRemover() instanceof Player) {
            Player player = (Player) event.getRemover();
            if (this.containsPlayer(player.getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onHangingPlace(HangingPlaceEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            ItemStack frame = event.getPlayer().getInventory().getItemInHand();
            int slot = event.getPlayer().getInventory().getHeldItemSlot();
            event.getPlayer().getInventory().setItem(slot, frame);
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onDrop(PlayerDropItemEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (event.getItemDrop().getItemStack().equals(this.minigameLeaveItem)) {
                event.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    private void onEntityExplode(EntityExplodeEvent event) {
        event.blockList().stream().forEach(block -> {
            if (this.getArena().getRegion().contains(block.getLocation())) {
                event.blockList().remove(block);
            }
        });
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onCreatureSpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.NATURAL)) {
            if (this.getArena().getRegion().contains(event.getLocation())) {
                /**
                 * If the entity is a monster
                 */
                if (event.getEntity() instanceof Monster) {
                    event.getEntity().remove();
                }

                /**
                 * If the entity is an animal
                 */
                else if (event.getEntity() instanceof Animals) {
                    event.getEntity().remove();
                }
            }
        }
    }

}
