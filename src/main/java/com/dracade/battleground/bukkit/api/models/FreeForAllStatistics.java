package com.dracade.battleground.bukkit.api.models;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.validation.NotNull;
import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.Bukkit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Optional;
import java.util.UUID;

@Entity()
@Table(name = "battleground_freeforall_statistics")
public class FreeForAllStatistics {

    @Id
    private int id;

    @NotNull
    @Column(name = "uniqueId", unique = true)
    private UUID uniqueId;

    @NotNull
    @Column(name = "totalKills")
    private int totalKills;

    @NotNull
    @Column(name = "totalDeaths")
    private int totalDeaths;

    @NotNull
    @Column(name = "totalTimePlayed")
    private long totalTimePlayed;

    private transient int kills;
    private transient int deaths;
    private transient int killstreak;

    private transient boolean isNewRecord;

    /**
     * Create a new FreeForAllStatistics instance.
     */
    public FreeForAllStatistics() {
        this.kills = 0;
        this.deaths = 0;
        this.killstreak = 0;

        this.isNewRecord = false;
    }

    /**
     * Set the id of the record.
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retrieve the record id.
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     * Set the player's id.
     *
     * @param uniqueId
     */
    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Retrieve the player's id.
     *
     * @return
     */
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Set the amount of kills the player has had in total.
     *
     * @param totalKills The amount of kills
     * @return The current amount of kills
     */
    public void setTotalKills(int totalKills) {
        this.totalKills = totalKills;
    }

    /**
     * Retrieve the amount of total kills.
     *
     * @return The amount of kills achieved
     */
    public int getTotalKills() {
        return this.totalKills;
    }

    /**
     * Set the amount of deaths the player has had in total.
     *
     * @param totalDeaths The amount of deaths
     * @return The current amount of deaths
     */
    public void setTotalDeaths(int totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    /**
     * Retrieve the amount of total deaths.
     *
     * @return The amount of deaths received
     */
    public int getTotalDeaths() {
        return this.totalDeaths;
    }

    /**
     * Set the amount of kills the player has had.
     *
     * @param kills The amount of kills
     * @return The current amount of kills
     */
    public void setKills(int kills) {
        this.kills = kills;
    }

    /**
     * Set the total play time.
     *
     * @param totalTimePlayed
     */
    public void setTotalTimePlayed(long totalTimePlayed) {
        this.totalTimePlayed = totalTimePlayed;
    }

    /**
     * Retrieve the total amount of time played.
     *
     * @return
     */
    public long getTotalTimePlayed() {
        return this.totalTimePlayed;
    }

    /**
     * Retrieve the amount of kills.
     *
     * @return The amount of kills achieved
     */
    public int getKills() {
        return this.kills;
    }

    /**
     * Set the amount of deaths the player has had.
     *
     * @param deaths The amount of deaths
     * @return The current amount of deaths
     */
    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    /**
     * Retrieve the amount of deaths.
     *
     * @return The amount of deaths received
     */
    public int getDeaths() {
        return this.deaths;
    }

    /**
     * Set the amount of kills the player has had without dying.
     *
     * @param killstreak The amount of kills
     * @return The current amount of kills without dying
     */
    public int setKillstreak(int killstreak) {
        return (this.killstreak = killstreak);
    }

    /**
     * Retrieve the amount of kills without dying.
     *
     * @return The amount of kills received
     */
    public int getKillstreak() {
        return this.killstreak;
    }

    /**
     * Set whether or not the daya is apart of a new record.
     *
     * @param isNewRecord
     */
    public void setNewRecord(boolean isNewRecord) {
        this.isNewRecord = isNewRecord;
    }

    /**
     * Determine if the data is apart of a new record.
     *
     * @return
     */
    public boolean isNewRecord() {
        return this.isNewRecord;
    }

    /**
     * Retrieve the kill/death ratio.
     *
     * @return The kill/death ratio
     */
    public double currentRatio() {
        double kills = this.kills;
        double deaths = this.deaths;

        kills = (kills <= 0) ? 1 : kills;
        deaths = (deaths <= 0) ? 1 : deaths;

        return (kills / deaths);
    }

    /**
     * Retrieve the total kill/death ratio.
     *
     * @return The kill/death ratio
     */
    public double totalRatio() {
        return this.totalRatio(false);
    }

    /**
     * Retrieve the total kill/death ratio.
     *
     * @return The kill/death ratio
     */
    public double totalRatio(boolean update) {
        double kills;
        double deaths;
        if (update) {
            kills = (this.totalKills + this.kills);
            deaths = (this.totalDeaths + this.deaths);
        } else {
            kills = (this.totalKills);
            deaths = (this.totalDeaths);
        }

        kills = (kills <= 0) ? 1 : kills;
        deaths = (deaths <= 0) ? 1 : deaths;

        return (kills / deaths);
    }

    /**
     * Data Access Object
     */
    public static class DAO {

        private static Cache<UUID, FreeForAllStatistics> cache = CacheBuilder.newBuilder()
                .weakKeys()
                .build();

        /**
         * Check if the value is cached.
         *
         * @param uniqueId
         * @return
         */
        public static boolean isCached(UUID uniqueId) {
            return DAO.cache.getIfPresent(uniqueId) != null;
        }

        /**
         * Get or create the statistics for the specified player.
         *
         * @param uniqueId The player's unique id
         * @return
         */
        public static FreeForAllStatistics getOrCreate(UUID uniqueId) {
            Optional<EbeanServer> database = BattlegroundBukkit.plugin().getDatabaseHandler().getDatabase();
            FreeForAllStatistics statistics = DAO.cache.getIfPresent(uniqueId);

            if (statistics == null) {
                // if a database connection exists
                if (database.isPresent()) {
                    statistics = database.get().find(FreeForAllStatistics.class).where().eq("uniqueId", uniqueId).findUnique();
                }

                // if the player's statistics weren't found
                if (statistics == null) {
                    statistics = new FreeForAllStatistics();

                    statistics.setUniqueId(uniqueId);
                    statistics.setTotalKills(0);
                    statistics.setTotalDeaths(0);
                    statistics.setKills(0);
                    statistics.setDeaths(0);
                    statistics.setKillstreak(0);
                    statistics.setNewRecord(true);
                }

            }
            return statistics;
        }

        /**
         * Save the specified statistics instance.
         *
         * @param statistics
         */
        public static void save(FreeForAllStatistics statistics) {
            Preconditions.checkNotNull(statistics);

            Optional<EbeanServer> database = BattlegroundBukkit.plugin().getDatabaseHandler().getDatabase();
            if (database.isPresent()) {
                if (statistics.isNewRecord()) {
                    database.get().save(statistics);
                } else {
                    database.get().update(statistics);
                }
            }

            DAO.cache.put(statistics.getUniqueId(), statistics);
        }

    }

}
