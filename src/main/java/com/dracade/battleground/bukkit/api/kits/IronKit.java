package com.dracade.battleground.bukkit.api.kits;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Optional;
import java.util.UUID;

public class IronKit extends BattlegroundKit {

    private ItemStack helmet;
    private ItemStack chestplate;
    private ItemStack leggings;
    private ItemStack boots;

    /**
     * Create an IronKit instance.
     *
     * @param uniqueId The player's unique id
     */
    public IronKit(UUID uniqueId) {
        super(uniqueId);

        this.helmet = new ItemStack(Material.IRON_HELMET);
        this.chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        this.leggings = new ItemStack(Material.IRON_LEGGINGS);
        this.boots = new ItemStack(Material.IRON_BOOTS);
    }

    @Override
    public Optional<Inventory> getInventory() {
        Inventory inventory = Bukkit.createInventory(null, InventoryType.PLAYER);

        ItemStack sword = new ItemStack(Material.IRON_SWORD);
        ItemStack fishingRod = new ItemStack(Material.FISHING_ROD);
        ItemStack flintAndSteel = new ItemStack(Material.FLINT_AND_STEEL, 1, (short) 64);
        ItemStack bow = new ItemStack(Material.BOW);
        ItemStack arrow = new ItemStack(Material.ARROW, 10);

        ItemMeta swordMeta = sword.getItemMeta();
        ItemMeta fishingRodMeta = fishingRod.getItemMeta();
        ItemMeta bowMeta = bow.getItemMeta();

        swordMeta.spigot().setUnbreakable(true);
        swordMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        fishingRodMeta.spigot().setUnbreakable(true);
        fishingRodMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        bowMeta.spigot().setUnbreakable(true);
        bowMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);

        sword.setItemMeta(swordMeta);
        fishingRod.setItemMeta(fishingRodMeta);
        bow.setItemMeta(bowMeta);

        inventory.setItem(0, sword);
        inventory.setItem(1, fishingRod);
        inventory.setItem(2, flintAndSteel);
        inventory.setItem(3, bow);
        inventory.setItem(4, arrow);

        Optional<ItemStack> minigameLeaveItem = this.getMinigameLeaveItem();
        if (minigameLeaveItem.isPresent()) {
            inventory.setItem(8, minigameLeaveItem.get());
        }

        return Optional.of(inventory);
    }

    @Override
    public Optional<ItemStack> getHelmet() {
        return Optional.of(this.helmet);
    }

    @Override
    public Optional<ItemStack> getChestplate() {
        return Optional.of(this.chestplate);
    }

    @Override
    public Optional<ItemStack> getLeggings() {
        return Optional.of(this.leggings);
    }

    @Override
    public Optional<ItemStack> getBoots() {
        return Optional.of(this.boots);
    }

}
