package com.dracade.battleground.bukkit.api.system;

import com.dracade.vone.bukkit.api.system.VoneQueueItem;
import com.dracade.vone.bukkit.api.utilities.PermissionWeightComparator;
import com.google.common.collect.ImmutableList;

import java.lang.ref.WeakReference;
import java.util.Optional;
import java.util.UUID;

public class CageMatchQueueItem implements VoneQueueItem {

    private static final String permissionEvaluator = "battleground.queue.priority.";

    private UUID holder;
    private UUID opponent;
    private int weight;
    private boolean interchangeable;

    /**
     * Create a queue item for a single player.
     *
     * @param holder
     */
    public CageMatchQueueItem(UUID holder) {
        this.holder = holder;
        this.weight = PermissionWeightComparator.getWeight(this.getQueuePermissionEvaluator(), this.getQueueHolder());
        this.interchangeable = true;
    }

    /**
     * Create a queue item between two players.
     *
     * @param holder
     * @param opponent
     */
    public CageMatchQueueItem(UUID holder, UUID opponent) {
        this(holder);
        this.opponent = opponent;
        this.interchangeable = false;
    }

    /**
     * Determine whether or not the opponents are
     * interchangeable.
     *
     * @return
     */
    public boolean isInterchangeable() {
        return this.interchangeable;
    }

    @Override
    public UUID getQueueHolder() {
        return this.holder;
    }

    @Override
    public ImmutableList<UUID> getPlayers() {
        ImmutableList.Builder<UUID> list = ImmutableList.builder();

        if (this.opponent != null) {
            list.add(this.opponent);
        }

        list.add(this.getQueueHolder());

        return list.build();
    }

    /**
     * Set the opponent of the holder.
     *
     * @param opponent
     */
    public void setOpponent(UUID opponent) {
        if (opponent != null) {
            int weightA = PermissionWeightComparator.getWeight(this.getQueuePermissionEvaluator(), this.getQueueHolder());
            int weightB = PermissionWeightComparator.getWeight(this.getQueuePermissionEvaluator(), opponent);

            this.weight = Math.max(weightA, weightB);
            this.opponent = opponent;
        }
    }

    /**
     * Retrieve the player's opponent.
     *
     * @return
     */
    public Optional<UUID> getOpponent() {
        return Optional.ofNullable(this.opponent);
    }

    @Override
    public String getQueuePermissionEvaluator() {
        return CageMatchQueueItem.permissionEvaluator;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }

}
