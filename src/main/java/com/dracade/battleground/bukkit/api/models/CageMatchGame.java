package com.dracade.battleground.bukkit.api.models;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.validation.NotNull;
import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.utilities.MatchmakingRating;
import com.google.common.base.Preconditions;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Optional;
import java.util.UUID;

@Entity()
@Table(name = "battleground_cagematch_games")
public class CageMatchGame {

    @Id
    private int id;

    @NotNull
    @Column(name = "uniqueId", unique = true)
    private UUID uniqueId;

    @NotNull
    @Column(name = "playerA")
    private UUID playerA;

    @NotNull
    @Column(name = "playerB")
    private UUID playerB;

    @NotNull
    @Column(name = "aScore")
    private int aScore;

    @NotNull
    @Column(name = "bScore")
    private int bScore;

    @NotNull
    @Column(name = "roundsPlayed")
    private int roundsPlayed;

    @NotNull
    @Column(name = "totalRounds")
    private int totalRounds;

    @Column(name = "forfeiter")
    private UUID forfeiter;

    @Column(name = "ranked")
    private boolean ranked;

    private transient boolean isNewRecord;

    /**
     * Create a new CageMatchGame instance.
     */
    public CageMatchGame() {
        this.playerA = null;
        this.playerB = null;
        this.aScore = 0;
        this.bScore = 0;
        this.roundsPlayed = 0;
        this.totalRounds = 0;
        this.forfeiter = null;
        this.isNewRecord = false;
        this.ranked = true;
    }

    /**
     * Set the id of the record.
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retrieve the record id.
     *
     * @return
     */
    public int getId() {
        return this.id;
    }

    /**
     * Set the game's id.
     *
     * @param uniqueId
     */
    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Retrieve the game's id.
     *
     * @return
     */
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Set player A's id.
     *
     * @param playerA
     */
    public void setPlayerA(UUID playerA) {
        this.playerA = playerA;
    }

    /**
     * Retrieve player A's id.
     *
     * @return
     */
    public UUID getPlayerA() {
        return this.playerA;
    }

    /**
     * Set player B's id.
     *
     * @param playerB
     */
    public void setPlayerB(UUID playerB) {
        this.playerB = playerB;
    }

    /**
     * Retrieve player B's id.
     *
     * @return
     */
    public UUID getPlayerB() {
        return this.playerB;
    }

    /**
     * Set player A's score.
     *
     * @param aScore
     */
    public void setAScore(int aScore) {
        this.aScore = aScore;
    }

    /**
     * Retrieve player A's score.
     *
     * @return
     */
    public int getAScore() {
        return this.aScore;
    }

    /**
     * Set player B's score.
     *
     * @param bScore
     */
    public void setBScore(int bScore) {
        this.bScore = bScore;
    }

    /**
     * Retrieve player B's score.
     *
     * @return
     */
    public int getBScore() {
        return this.bScore;
    }

    /**
     * Set the amount of rounds played.
     *
     * @param roundsPlayed
     */
    public void setRoundsPlayed(int roundsPlayed) {
        this.roundsPlayed = roundsPlayed;
    }

    /**
     * Retrieve the amount of rounds played.
     *
     * @return
     */
    public int getRoundsPlayed() {
        return this.roundsPlayed;
    }

    /**
     * Set the total amount of rounds.
     *
     * @param totalRounds
     */
    public void setTotalRounds(int totalRounds) {
        this.totalRounds = totalRounds;
    }

    /**
     * Retrieve the total amount of rounds.
     *
     * @return
     */
    public int getTotalRounds() {
        return this.totalRounds;
    }

    /**
     * Set the player's id who forfeited
     *
     * @param forfeiter
     */
    public void setForfeiter(UUID forfeiter) {
        this.forfeiter = forfeiter;
    }

    /**
     * Retrieve the player who forfeited.
     *
     * @return
     */
    public UUID getForfeiter() {
        return this.forfeiter;
    }

    /**
     * Determine whether or not the game was forfeited.
     *
     * @return
     */
    public boolean wasForfeited() {
        return this.forfeiter != null;
    }

    /**
     * Set whether or not the game was ranked.
     *
     * @param ranked
     */
    public void setRanked(boolean ranked) {
        this.ranked = ranked;
    }

    /**
     * Determine whether or not the game was ranked.
     *
     * @return
     */
    public boolean getRanked() {
        return this.ranked;
    }

    /**
     * Set whether or not the daya is apart of a new record.
     *
     * @param isNewRecord
     */
    public void setNewRecord(boolean isNewRecord) {
        this.isNewRecord = isNewRecord;
    }

    /**
     * Determine if the data is apart of a new record.
     *
     * @return
     */
    public boolean isNewRecord() {
        return this.isNewRecord;
    }

    /**
     * Data Access Object
     */
    public static class DAO {

        /**
         * Get or create the statistics for the specified player.
         *
         * @param uniqueId The player's unique id
         * @return
         */
        public static CageMatchGame getOrCreate(UUID uniqueId) {
            Optional<EbeanServer> database = BattlegroundBukkit.plugin().getDatabaseHandler().getDatabase();
            CageMatchGame statistics = null;

            // if a database connection exists
            if (database.isPresent()) {
                statistics = database.get().find(CageMatchGame.class).where().eq("uniqueId", uniqueId).findUnique();
            }

            // if the player's statistics weren't found
            if (statistics == null) {
                statistics = new CageMatchGame();

                statistics.setUniqueId(uniqueId);
                statistics.setNewRecord(true);
            }

            return statistics;
        }

        /**
         * Save the specified statistics instance.
         *
         * @param statistics
         */
        public static void save(CageMatchGame statistics) {
            Preconditions.checkNotNull(statistics);

            Optional<EbeanServer> database = BattlegroundBukkit.plugin().getDatabaseHandler().getDatabase();
            if (database.isPresent()) {
                if (statistics.isNewRecord()) {
                    database.get().save(statistics);
                } else {
                    database.get().update(statistics);
                }
            }
        }

    }

}

