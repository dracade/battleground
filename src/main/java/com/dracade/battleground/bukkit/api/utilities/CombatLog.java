package com.dracade.battleground.bukkit.api.utilities;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class CombatLog implements Listener {

    private static CombatLog instance;

    /**
     * Retrieve the CombatLog instance.
     *
     * @return
     */
    public static CombatLog getInstance() {
        return (CombatLog.instance != null) ? (CombatLog.instance) : (CombatLog.instance = new CombatLog());
    }

    private int cooldown;
    private Map<UUID, Long> hitLog;
    private Map<UUID, UUID> lastAttackingPlayer;

    /**
     * Create a new CombatLog instance.
     */
    private CombatLog() {
        this.cooldown = 7;
        this.hitLog = new HashMap<UUID, Long>();
        this.lastAttackingPlayer = new HashMap<UUID, UUID>();
        Bukkit.getPluginManager().registerEvents(this, BattlegroundBukkit.plugin());
    }

    /**
     * Updates the player's last combat hit.
     *
     * @param playerId
     */
    public void update(UUID playerId, UUID lastAttacker) {
        this.hitLog.put(playerId, System.currentTimeMillis());
        this.hitLog.put(lastAttacker, System.currentTimeMillis());

        this.lastAttackingPlayer.put(playerId, lastAttacker);
        this.lastAttackingPlayer.put(lastAttacker, playerId);
    }

    /**
     * Check if the player is logged.
     *
     * @param playerId
     * @return
     */
    public boolean check(UUID playerId) {
        if (this.hitLog.get(playerId) != null) {
            if (this.hitLog.get(playerId) > (System.currentTimeMillis() - (this.cooldown * 1000))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove a player from the log.
     *
     * @param playerId
     */
    public void remove(UUID playerId) {
        this.hitLog.remove(playerId);
        this.lastAttackingPlayer.remove(playerId);
    }

    @EventHandler
    private void onPlayerQuit(PlayerQuitEvent event) {
        this.remove(event.getPlayer().getUniqueId());
    }

    /**
     * Retrieve the last attacking player.
     *
     * @param playerId
     * @return
     */
    public Optional<UUID> getLastAttackingPlayer(UUID playerId) {
        return Optional.ofNullable(this.lastAttackingPlayer.get(playerId));
    }

}
