package com.dracade.battleground.bukkit.api.events;

import com.dracade.battleground.bukkit.plugin.BattlegroundLobby;
import org.bukkit.event.Event;

public abstract class LobbyEvent extends Event {

    private BattlegroundLobby lobby;

    /**
     * Create a new LobbyEvent instance.
     *
     * @param lobby The lobby instance that is associated with the event
     */
    protected LobbyEvent(BattlegroundLobby lobby) {
        this.lobby = lobby;
    }

    /**
     * Retrieve the associated lobby.
     *
     * @return BattlegroundLobby instance
     */
    public BattlegroundLobby getLobby() {
        return this.lobby;
    }

}
