package com.dracade.battleground.bukkit.api.events.lobby;

import com.dracade.battleground.bukkit.plugin.BattlegroundLobby;
import com.dracade.battleground.bukkit.api.events.LobbyEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class LobbyJoinEvent extends LobbyEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return handlers;
    }

    private boolean cancelled;
    private Player player;

    /**
     * Create a new LobbyJoinEvent instance.
     *
     * @param lobby The lobby that is associated with the event
     */
    public LobbyJoinEvent(BattlegroundLobby lobby, Player player) {
        super(lobby);
        this.cancelled = false;
        this.player = player;
    }

    @Override
    public HandlerList getHandlers() {
        return LobbyJoinEvent.handlers;
    }

    /**
     * Retrieve the player.
     *
     * @return The player instance
     */
    public Player getPlayer() {
        return this.player;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

}
