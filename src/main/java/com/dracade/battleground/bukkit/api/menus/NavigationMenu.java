package com.dracade.battleground.bukkit.api.menus;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.minigames.FreeForAll;
import com.dracade.battleground.bukkit.api.models.CageMatchStatistics;
import com.dracade.battleground.bukkit.api.system.CageMatchQueue;
import com.dracade.battleground.bukkit.api.system.CageMatchQueueItem;
import com.dracade.battleground.bukkit.api.utilities.Helper;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Menu;
import com.dracade.vone.bukkit.api.utilities.FancyMenu;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NavigationMenu implements Menu {

    /**
     * Open the NavigationMenu for the specified player.
     *
     * @param player
     */
    public void open(Player player) {
        this.open(player, null);
    }

    @Override
    public void open(Player player, Menu menu) {
        Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

        String title = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "navigation-menu.inventory-title");

        FancyMenu fancyMenu = new FancyMenu(Bukkit.createInventory(null, 9, title));

        /**
         * Set the FreeForAll menu action
         */
        fancyMenu.setAction(0, () -> {
            new MinigameMenu(FreeForAll.class).open(player);
        });
        fancyMenu.getInventory().setItem(0, getFreeForAllItem(locale));

        /**
         * Set the CageMatch menu action
         */
        fancyMenu.setAction(1, () -> {
            CageMatchQueue queue = CageMatchQueue.getInstance();
            if (queue.contains(player.getUniqueId())) {
                queue.remove(player.getUniqueId());
                player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.menu-info.removed-from-queue"));
                player.closeInventory();
            } else {
                queue.add(new CageMatchQueueItem(player.getUniqueId()));
                player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.menu-info.added-to-queue"));
                player.closeInventory();
            }
        });
        fancyMenu.getInventory().setItem(1, getCageMatchItem(player, locale));

        // Open the inventory
        fancyMenu.open(player);
    }

    /**
     * Retrieve the freeforall menu item.
     *
     * @return ItemStack
     */
    private ItemStack getFreeForAllItem(Locale locale) {
        ItemStack item = new ItemStack(Material.BOW, 1);
        ItemMeta itemMeta = item.getItemMeta();

        String displayName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.menu-info.display-name");
        String itemLore = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.freeforall.menu-info.display-lore");

        itemMeta.setDisplayName(displayName);

        if (!itemLore.isEmpty()) {
            itemMeta.setLore(Helper.splitWhereNewline(itemLore));
        }

        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(itemMeta);

        return item;
    }

    /**
     * Retrieve the freeforall menu item.
     *
     * @return ItemStack
     */
    private ItemStack getCageMatchItem(Player player, Locale locale) {
        ItemStack item = new ItemStack(Material.IRON_SWORD, 1);
        ItemMeta itemMeta = item.getItemMeta();

        String displayName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.menu-info.display-name");
        String itemLore = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.menu-info.display-lore");

        itemMeta.setDisplayName(displayName);

        if (CageMatchQueue.getInstance().contains(player.getUniqueId())) {
            itemLore = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.menu-info.display-queue-lore");
            itemMeta.addEnchant(Enchantment.LUCK, 1, true);
        }

        if (!itemLore.isEmpty()) {
            itemMeta.setLore(Helper.splitWhereNewline(itemLore));
        }

        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(itemMeta);

        return item;
    }
}
