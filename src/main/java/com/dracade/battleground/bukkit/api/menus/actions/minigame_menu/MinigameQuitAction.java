package com.dracade.battleground.bukkit.api.menus.actions.minigame_menu;

import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.game.Playable;
import org.bukkit.entity.Player;

import java.util.Optional;

public class MinigameQuitAction implements Runnable {

    private Player player;

    /**
     * Create a new MinigameQuitAction.
     *
     * @param player
     */
    public MinigameQuitAction(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        Optional<Minigame> minigame = VoneBukkit.plugin().registry().getMinigame(this.player);
        if (minigame.isPresent()) {
            if (minigame.get() instanceof Playable) {
                ((Playable) minigame.get()).removePlayer(this.player.getUniqueId());
            }
        }
    }

}
