package com.dracade.battleground.bukkit.api.menus;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.menus.actions.minigame_menu.MinigameJoinAction;
import com.dracade.battleground.bukkit.api.utilities.Helper;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Menu;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.utilities.FancyMenu;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class MinigameMenu <T extends Minigame> implements Menu {

    private Class<T> filter;

    /**
     * Create a new MinigameMenu instance.
     *
     * @param filter
     */
    public MinigameMenu(Class<T> filter) {
        this.filter = filter;
    }

    @Override
    public void open(Player player) {
        this.open(player, null);
    }

    @Override
    public void open(Player player, Menu menu) {
        Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

        long size = ((VoneBukkit.plugin().registry().getMinigames().stream().filter(minigame -> minigame.getClass().isAssignableFrom(this.filter)).count() % 9) * 9);

        String title = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigame-menu.inventory-title")
                .replace("{{FILTER}}", filter.getSimpleName());

        FancyMenu fancyMenu = new FancyMenu(Bukkit.createInventory(null, (int) ((size <= 0) ? 9 : size), title));

        VoneBukkit.plugin().registry().getMinigames().stream().forEach(minigame -> {
            if (minigame.getClass().isAssignableFrom(this.filter)) {
                ItemStack item = new ItemStack(Material.WOOL);
                ItemMeta itemMeta = item.getItemMeta();

                String i18nPrefix = minigame.getClass().getSimpleName().toLowerCase();

                String itemDisplayName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames." + i18nPrefix + ".menu-info.display-name");
                String itemLore = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigame-menu." + i18nPrefix + ".display-lore");

                if (!itemDisplayName.isEmpty()) {
                    itemMeta.setDisplayName(itemDisplayName);
                }

                if (!itemLore.isEmpty()) {
                    List<String> itemLoreList = new ArrayList<String>();
                    for (String s : Helper.splitWhereNewline(itemLore)) {
                        itemLoreList.add(s
                                .replace("{{ARENA_NAME}}", minigame.getArena().getName())
                                .replace("{{ARENA_DISPLAY_NAME}}", minigame.getArena().getDisplayName())
                                .replace("{{PLAYERS}}", String.valueOf(minigame.getPlayers().size()))
                                .trim());
                    }
                    itemMeta.setLore(itemLoreList);
                }

                itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                item.setItemMeta(itemMeta);

                int slot = fancyMenu.getActions().size();
                fancyMenu.getInventory().setItem(slot, item);
                fancyMenu.setAction(slot, new MinigameJoinAction(minigame, player));
            }
        });

        fancyMenu.open(player);
    }

}
