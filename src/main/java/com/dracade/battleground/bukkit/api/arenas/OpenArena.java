package com.dracade.battleground.bukkit.api.arenas;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.core.api.Request;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.utilities.FancyMessage;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

public final class OpenArena extends Arena {

    private Map<Integer, Location> locations;

    /**
     * Create a OpenArena instance.
     *
     * @param name The name of the arena
     * @param spawn The main spawn location
     */
    public OpenArena(String name, Location spawn) {
        super(name, spawn);

        this.locations = new HashMap<Integer, Location>();
    }

    /**
     * Add a location.
     *
     * @param location The location you wish to add
     * @return True if the location was added successfully
     */
    public boolean addLocation(Location location) {
        if (!this.locations.containsValue(location)) {
            // TODO: refactor ...
            int id = 0;
            if (this.locations.size() > 0) {
                Map.Entry<Integer, Location> lastEntry = new ArrayList<Map.Entry<Integer, Location>>(this.locations.entrySet()).get(this.locations.size() - 1);
                id = lastEntry.getKey();
            }

            this.locations.put(id + 1, location);

            return true;
        }
        return false;
    }

    /**
     * Remove a spawn location.
     *
     * @param location The location you wish to remove
     * @return True if the location was removed successfully
     */
    public boolean removeLocation(Location location) {
        if (this.locations.containsValue(location)) {
            this.locations.entrySet().stream()
                    .filter(l -> l.getValue().equals(location))
                    .forEach(l -> this.locations.remove(l.getKey()));
            return true;
        }
        return false;
    }

    /**
     * Remove a spawn location.
     *
     * @param id The location's id
     * @return True if the location was removed successfully
     */
    public boolean removeLocation(int id) {
        if (this.locations.containsKey(id)) {
            this.locations.remove(id);
            return true;
        }
        return false;
    }

    /**
     * Get a list of all of the available locations.
     *
     * @return A list of locations
     */
    public ImmutableMap<Integer, Location> getLocations() {
        return (this.locations.size() > 0) ? ImmutableMap.copyOf(this.locations) : ImmutableMap.of(0, this.getSpawnLocation());
    }

    /**
     * Get a random location.
     *
     * @return A random location
     */
    public Location getRandomLocation() {
        return (this.locations.size() > 0) ? new ArrayList<Location>(this.locations.values()).get(new Random().nextInt(this.locations.size() - 1)) : this.getSpawnLocation();
    }

    /**
     * Vone command editor
     */
    public static class CommandEditor implements VoneEditor {

        @Override
        public <T extends Arena> Class<T> getArenaType() {
            return (Class<T>) OpenArena.class;
        }

        @Override
        public ImmutableSet<String> getCreateExecutorUsage() {
            return ImmutableSet.of(
                    "<name>"
            );
        }

        @Override
        public ImmutableSet<String> getEditExecutorUsage() {
            return ImmutableSet.of(
                    "set name <name>",
                    "set displayname <name>",
                    "set spawn",
                    "set animals",
                    "set monsters",
                    "set region a",
                    "set region b",
                    "set region ignore-y",
                    "add location",
                    "remove location <id>"
            );
        }

        @Override
        public ImmutableSet<String> getInfoExecutorUsage() {
            return ImmutableSet.of(
                    "",
                    "locations"
            );
        }

        @Override
        public Optional<Arena> onCreateExecutor(Player player, String[] args) throws ArrayIndexOutOfBoundsException {
            if (args.length > 0) {
                return Optional.of(new OpenArena(args[0], player.getLocation()));
            }
            return Optional.empty();
        }

        @Override
        public boolean onEditExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException {
            final Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

            /**
             * Add to the arena
             */
            if (args[0].equalsIgnoreCase("add")) {

                /**
                 * Add a location to the arena
                 */
                if (args[1].equalsIgnoreCase("location")) {
                    ((OpenArena) arena).addLocation(player.getLocation());

                    List<Integer> locations = ((OpenArena) arena).getLocations().keySet().asList();
                    int id = locations.get(locations.size() - 1);

                    String message = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.location-added")
                            .replace("{{LOCATION_ID}}", String.valueOf(id));

                    player.sendMessage(message);
                }

                /**
                 * Incorrect usage
                 */
                else {
                    return false;
                }

            }

            /**
             * Remove from the arena
             */
            else if (args[0].equalsIgnoreCase("remove")) {

                /**
                 * Remove a location from the arena
                 */
                if (args[1].equalsIgnoreCase("location")) {
                    try {
                        boolean success = ((OpenArena) arena).removeLocation(Integer.parseInt(args[2]));
                        if (success) {
                            String message = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.location-removed")
                                    .replace("{{LOCATION_ID}}", args[2]);

                            player.sendMessage(message);
                        } else {
                            player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.arenas.openarena.invalid-location-id"));
                        }
                    } catch (NumberFormatException e) {
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.arenas.openarena.invalid-location-id"));
                    }
                }

                /**
                 * Incorrect usage
                 */
                else {
                    return false;
                }

            }

            /**
             * Set ...
             */
            else if (args[0].equalsIgnoreCase("set")) {

                /**
                 * Set the name of the arena
                 */
                if (args[1].equalsIgnoreCase("name")) {
                    arena.setName(args[2]);

                    String message = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.name-set")
                            .replace("{{ARENA_NAME}}", arena.getName());

                    player.sendMessage(message);
                }

                /**
                 * Set the display name of the arena
                 */
                else if (args[1].equalsIgnoreCase("displayname")) {
                    String displayName = "";
                    for (int i = 2; i < args.length; i++) {
                        displayName += args[i];
                        if (i < (args.length - 1)) {
                            displayName += " ";
                        }
                    }

                    arena.setDisplayName(displayName);

                    String message = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.display-name-set")
                            .replace("{{ARENA_DISPLAY_NAME}}", arena.getDisplayName());

                    player.sendMessage(message);
                }

                /**
                 * Set the spawn location of the arena
                 */
                else if (args[1].equalsIgnoreCase("spawn")) {
                    arena.setSpawnLocation(player.getLocation());
                    player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.spawn-set"));
                }

                /**
                 * Set whether or not the arena can spawn animals
                 */
                else if (args[1].equalsIgnoreCase("animals")) {
                    // TODO:
                }

                /**
                 * Set whether or not the arena can spawn monsters
                 */
                else if (args[1].equalsIgnoreCase("monsters")) {
                    // TODO:
                }

                /**
                 * Set the container variables
                 */
                else if (args[1].equalsIgnoreCase("region")) {
                    /**
                     * Set the container A value
                     */
                    if (args[2].equalsIgnoreCase("a")) {
                        arena.getRegion().setA(player.getTargetBlock((Set) null, 5).getLocation());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.region-a-set"));
                    }

                    /**
                     * Set the container B value
                     */
                    else if (args[2].equalsIgnoreCase("b")) {
                        arena.getRegion().setB(player.getTargetBlock((Set) null, 5).getLocation());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.region-b-set"));
                    }

                    /**
                     * Set the container to ignore y axis values
                     */
                    else if (args[2].equalsIgnoreCase("ignore-y")) {
                        arena.getRegion().setIgnoreY(!arena.getRegion().isIgnoreY());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.region-ignore-y-set").replace("{{VALUE}}", String.valueOf(arena.getRegion().isIgnoreY())));
                    }

                    /**
                     * Incorrect usage
                     */
                    else {
                        return false;
                    }

                }

                /**
                 * Incorrect usage
                 */
                else {
                    return false;
                }

            }

            /**
             * Incorrect usage
             */
            else {
                return false;
            }

            return true;
        }

        @Override
        public boolean onInfoExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException {
            final Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

            if (args.length > 0) {
                /**
                 * List locations info
                 */
                if (args[0].equalsIgnoreCase("locations")) {
                    player.sendMessage("");
                    ((OpenArena) arena).getLocations().entrySet().stream().forEach(location -> {
                        FancyMessage.PlaceholderMessage message = new FancyMessage.PlaceholderMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.location-list-format"));

                        /**
                         * Add a {{LOCATION_ID}} placeholder.
                         */
                        message.placeholder("{{LOCATION_ID}}", (fancyMessage, chatColor) -> {
                            Request.Handler.create(new Request() {
                                @Override
                                public void onCreate(UUID uuid) {
                                    fancyMessage.then(location.getKey().toString())
                                            .tooltip(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.openarena.location-list-tooltip"))
                                            .command("/vone request accept " + uuid.toString())
                                            .color(chatColor);
                                }

                                @Override
                                public void onAccept() {
                                    player.teleport(location.getValue());
                                }

                                @Override
                                public void onDecline() {
                                    // do nothing ...
                                }
                            });
                        });

                        /**
                         * Add a {{LOCATION_X}} placeholder.
                         */
                        message.placeholder("{{LOCATION_X}}", (fancyMessage, chatColor) -> {
                            fancyMessage.then(String.format("%.2f", location.getValue().getZ())).color(chatColor);
                        });

                        /**
                         * Add a {{LOCATION_Y}} placeholder.
                         */
                        message.placeholder("{{LOCATION_Y}}", (fancyMessage, chatColor) -> {
                            fancyMessage.then(String.format("%.2f", location.getValue().getY())).color(chatColor);
                        });

                        /**
                         * Add a {{LOCATION_Z}} placeholder.
                         */
                        message.placeholder("{{LOCATION_Z}}", (fancyMessage, chatColor) -> {
                            fancyMessage.then(String.format("%.2f", location.getValue().getZ())).color(chatColor);
                        });

                        message.build().send(player);
                    });
                    player.sendMessage("");

                    return true;
                }

                return false;
            }

            String arenaName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "settings.arena-info.openarena.name")
                    .replace("{{ARENA_NAME}}", arena.getName());

            String arenaDisplayName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "settings.arena-info.openarena.display-name")
                    .replace("{{ARENA_DISPLAY_NAME}}", arena.getDisplayName());

            player.sendMessage("");
            player.sendMessage("  " + arenaName);
            player.sendMessage("  " + arenaDisplayName);
            player.sendMessage("");

            return true;
        }

        @Override
        public Optional<Arena> onSaveExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException {
            return Optional.of(arena);
        }

    }

}
