package com.dracade.battleground.bukkit.api.events.lobby;

import com.dracade.battleground.bukkit.plugin.BattlegroundLobby;
import com.dracade.battleground.bukkit.api.events.LobbyEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public class LobbyInitEvent extends LobbyEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Retrieve the handlers associated with this event.
     *
     * @return A list of event handlers
     */
    public static HandlerList getHandlerList() {
        return handlers;
    }

    private Player player;

    /**
     * Create a new LobbyInitEvent instance.
     *
     * @param lobby The lobby that is associated with the event
     */
    public LobbyInitEvent(BattlegroundLobby lobby, Player player) {
        super(lobby);
        this.player = player;
    }

    @Override
    public HandlerList getHandlers() {
        return LobbyInitEvent.handlers;
    }

    /**
     * Retrieve the player.
     *
     * @return The player instance
     */
    public Player getPlayer() {
        return this.player;
    }

}
