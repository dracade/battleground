package com.dracade.battleground.bukkit.api.arenas;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Arena;
import com.dracade.vone.bukkit.api.system.VoneEditor;
import com.dracade.vone.bukkit.api.utilities.FancyMessage;
import com.dracade.vone.core.api.Request;
import com.google.common.collect.ImmutableSet;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

public class CageArena extends Arena {

    private Location a;
    private Location b;

    /**
     * Create a CageArena instance.
     *
     * @param name
     * @param spawn
     */
    public CageArena(String name, Location spawn) {
        this(name, spawn, spawn, spawn);
    }

    /**
     * Create a CageArena instance.
     *
     * @param name
     * @param spawn
     * @param a
     * @param b
     */
    public CageArena(String name, Location spawn, Location a, Location b) {
        super(name, spawn);
        this.a = a;
        this.b = b;
    }

    /**
     * Set the A spawn location.
     *
     * @param a
     */
    public void setA(Location a) {
        this.a = a;
    }

    /**
     * Get the A spawn location.
     *
     * @return
     */
    public Location getA() {
        return (this.a != null) ? this.a : this.getSpawnLocation();
    }

    /**
     * Set the B spawn location.
     *
     * @param b
     */
    public void setB(Location b) {
        this.b = b;
    }

    /**
     * Get the B spawn location.
     *
     * @return
     */
    public Location getB() {
        return (this.b != null) ? this.b : this.getSpawnLocation();
    }

    /**
     * Vone Command Editor
     */
    public static class CommandEditor implements VoneEditor {

        @Override
        public <T extends Arena> Class<T> getArenaType() {
            return (Class<T>) CageArena.class;
        }

        @Override
        public ImmutableSet<String> getCreateExecutorUsage() {
            return ImmutableSet.of(
                    "<name>"
            );
        }

        @Override
        public ImmutableSet<String> getEditExecutorUsage() {
            return ImmutableSet.of(
                    "set name <name>",
                    "set displayname <name>",
                    "set spawn",
                    "set animals",
                    "set monsters",
                    "set region a",
                    "set region b",
                    "set region ignore-y",
                    "set location a",
                    "set location b"
            );
        }

        @Override
        public ImmutableSet<String> getInfoExecutorUsage() {
            return ImmutableSet.of(
                    "",
                    "locations"
            );
        }

        @Override
        public Optional<Arena> onCreateExecutor(Player player, String[] args) throws ArrayIndexOutOfBoundsException {
            if (args.length > 0) {
                return Optional.of(new CageArena(args[0], player.getLocation()));
            }
            return Optional.empty();
        }

        @Override
        public boolean onEditExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException {
            final Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

            /**
             * Set ...
             */
            if (args[0].equalsIgnoreCase("set")) {

                /**
                 * Set the name of the arena
                 */
                if (args[1].equalsIgnoreCase("name")) {
                    arena.setName(args[2]);

                    String message = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.name-set")
                            .replace("{{ARENA_NAME}}", arena.getName());

                    player.sendMessage(message);
                }

                /**
                 * Set the display name of the arena
                 */
                else if (args[1].equalsIgnoreCase("displayname")) {
                    String displayName = "";
                    for (int i = 2; i < args.length; i++) {
                        displayName += args[i];
                        if (i < (args.length - 1)) {
                            displayName += " ";
                        }
                    }

                    arena.setDisplayName(displayName);

                    String message = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.display-name-set")
                            .replace("{{ARENA_DISPLAY_NAME}}", arena.getDisplayName());

                    player.sendMessage(message);
                }

                /**
                 * Set the spawn location of the arena
                 */
                else if (args[1].equalsIgnoreCase("spawn")) {
                    arena.setSpawnLocation(player.getLocation());
                    player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.spawn-set"));
                }

                /**
                 * Set whether or not the arena can spawn animals
                 */
                else if (args[1].equalsIgnoreCase("animals")) {
                    // TODO:
                }

                /**
                 * Set whether or not the arena can spawn monsters
                 */
                else if (args[1].equalsIgnoreCase("monsters")) {
                    // TODO:
                }

                /**
                 * Set the container variables
                 */
                else if (args[1].equalsIgnoreCase("region")) {
                    /**
                     * Set the container A value
                     */
                    if (args[2].equalsIgnoreCase("a")) {
                        arena.getRegion().setA(player.getTargetBlock((Set) null, 5).getLocation());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.region-a-set"));
                    }

                    /**
                     * Set the container B value
                     */
                    else if (args[2].equalsIgnoreCase("b")) {
                        arena.getRegion().setB(player.getTargetBlock((Set) null, 5).getLocation());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.region-b-set"));
                    }

                    /**
                     * Set the container to ignore y axis values
                     */
                    else if (args[2].equalsIgnoreCase("ignore-y")) {
                        arena.getRegion().setIgnoreY(!arena.getRegion().isIgnoreY());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.region-ignore-y-set").replace("{{VALUE}}", String.valueOf(arena.getRegion().isIgnoreY())));
                    }

                    /**
                     * Incorrect usage
                     */
                    else {
                        return false;
                    }

                }

                /**
                 * Set the location values
                 */
                else if (args[1].equalsIgnoreCase("location")) {
                    /**
                     * Set the location A value
                     */
                    if (args[2].equalsIgnoreCase("a")) {
                        ((CageArena) arena).setA(player.getLocation());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.location-a-set"));
                    }

                    /**
                     * Set the location B value
                     */
                    else if (args[2].equalsIgnoreCase("b")) {
                        ((CageArena) arena).setB(player.getLocation());
                        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.location-b-set"));
                    }

                    /**
                     * Incorrect usage
                     */
                    else {
                        return false;
                    }
                }

                /**
                 * Incorrect usage
                 */
                else {
                    return false;
                }

            }

            /**
             * Incorrect usage
             */
            else {
                return false;
            }

            return true;
        }

        @Override
        public Optional<Arena> onSaveExecutor(Player player, Arena arena, String[] strings) throws ArrayIndexOutOfBoundsException {
            return Optional.of(arena);
        }

        @Override
        public boolean onInfoExecutor(Player player, Arena arena, String[] args) throws ArrayIndexOutOfBoundsException {
            final Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

            if (args.length > 0) {
                /**
                 * List locations info
                 */
                if (args[0].equalsIgnoreCase("locations")) {
                    player.sendMessage("");

                    Location a = ((CageArena) arena).getA();
                    Location b = ((CageArena) arena).getB();

                    this.sendLocationInfo(player, "a", a);
                    this.sendLocationInfo(player, "b", b);
                }
                player.sendMessage("");

                return true;
            }

            String arenaName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "settings.arena-info.cagearena.name")
                    .replace("{{ARENA_NAME}}", arena.getName());

            String arenaDisplayName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "settings.arena-info.cagearena.display-name")
                    .replace("{{ARENA_DISPLAY_NAME}}", arena.getDisplayName());

            player.sendMessage("");
            player.sendMessage("  " + arenaName);
            player.sendMessage("  " + arenaDisplayName);
            player.sendMessage("");

            return true;
        }

        /**
         * Utility method to send location info the player.
         *
         * @param player
         * @param name
         * @param location
         */
        private void sendLocationInfo(Player player, String name, Location location) {
            final Locale locale = VoneBukkit.plugin().getPlayerLocale(player);
            FancyMessage.PlaceholderMessage message = new FancyMessage.PlaceholderMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.location-list-format"));

            /**
             * Add a {{LOCATION_ID}} placeholder.
             */
            message.placeholder("{{LOCATION_ID}}", (fancyMessage, chatColor) -> {
                Request.Handler.create(new Request() {
                    @Override
                    public void onCreate(UUID uuid) {
                        fancyMessage.then(name)
                                .tooltip(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.arenas.cagearena.location-list-tooltip"))
                                .command("/vone request accept " + uuid.toString())
                                .color(chatColor);
                    }

                    @Override
                    public void onAccept() {
                        player.teleport(location);
                    }

                    @Override
                    public void onDecline() {
                        // do nothing ...
                    }
                });
            });

            /**
             * Add a {{LOCATION_X}} placeholder.
             */
            message.placeholder("{{LOCATION_X}}", (fancyMessage, chatColor) -> {
                fancyMessage.then(String.format("%.2f", location.getZ())).color(chatColor);
            });

            /**
             * Add a {{LOCATION_Y}} placeholder.
             */
            message.placeholder("{{LOCATION_Y}}", (fancyMessage, chatColor) -> {
                fancyMessage.then(String.format("%.2f", location.getY())).color(chatColor);
            });

            /**
             * Add a {{LOCATION_Z}} placeholder.
             */
            message.placeholder("{{LOCATION_Z}}", (fancyMessage, chatColor) -> {
                fancyMessage.then(String.format("%.2f", location.getZ())).color(chatColor);
            });

            message.build().send(player);
        }
    }
}
