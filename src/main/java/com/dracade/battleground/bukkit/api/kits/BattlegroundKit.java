package com.dracade.battleground.bukkit.api.kits;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Kit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

public abstract class BattlegroundKit implements Kit, Listener {

    private transient UUID uniqueId;
    private transient ItemStack minigameLeaveItem;

    /**
     * Create a new BattlegroundKit.
     *
     * @param uniqueId the player's unique id
     */
    public BattlegroundKit(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Retrieve the unique id of the kit owner.
     *
     * @return
     */
    public UUID getOwnerId() {
        return this.uniqueId;
    }

    /**
     * Set the minigame leave item.
     *
     * @param minigameLeaveItem
     */
    public BattlegroundKit setMinigameLeaveItem(ItemStack minigameLeaveItem) {
        this.minigameLeaveItem = minigameLeaveItem;
        return this;
    }

    /**
     * Retrieve the minigame leave item.
     *
     * @return
     */
    public Optional<ItemStack> getMinigameLeaveItem() {
        Optional<Player> player = this.getPlayer();

        Locale locale;

        if (player.isPresent()) {
            locale = VoneBukkit.plugin().getPlayerLocale(player.get());
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        ItemMeta minigameLeaveItemMeta = minigameLeaveItem.getItemMeta();

        String minigameLeaveItemName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.leave-item-name");

        if (!minigameLeaveItemName.isEmpty()) {
            minigameLeaveItemMeta.setDisplayName(minigameLeaveItemName);
        }

        minigameLeaveItemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        this.minigameLeaveItem.setItemMeta(minigameLeaveItemMeta);

        return (this.minigameLeaveItem != null) ? Optional.of(minigameLeaveItem) : Optional.empty();
    }

    /**
     * Retrieve the owner of this kit.
     *
     * @return
     */
    public Optional<Player> getPlayer() {
        Player player = Bukkit.getPlayer(this.uniqueId);
        return (player != null) ? Optional.of(player) : Optional.empty();
    }

    /**
     * Give the kit to it's owner.
     */
    public void give() {
        Optional<Player> player = this.getPlayer();
        if (player.isPresent()) {
            this.give(player.get());
        }
    }

}
