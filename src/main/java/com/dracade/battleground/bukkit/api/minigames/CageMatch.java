package com.dracade.battleground.bukkit.api.minigames;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.arenas.CageArena;
import com.dracade.battleground.bukkit.api.kits.BattlegroundKit;
import com.dracade.battleground.bukkit.api.kits.IronKit;
import com.dracade.battleground.bukkit.api.models.CageMatchGame;
import com.dracade.battleground.bukkit.api.models.CageMatchStatistics;
import com.dracade.battleground.bukkit.api.models.FreeForAllStatistics;
import com.dracade.battleground.bukkit.plugin.BattlegroundConnect;
import com.dracade.battleground.bukkit.plugin.BattlegroundLobby;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.events.minigame.MinigameJoinEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameLoadedEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameQuitEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameStartedEvent;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.utilities.FancyScoreboard;
import com.google.common.collect.ImmutableList;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class CageMatch implements Minigame {

    private UUID uniqueId;

    private CageArena arena;

    private DataContainer a;
    private DataContainer b;

    private RoundResult[] rounds;
    private RoundState roundState;
    private int currentRound;

    private int currentTime;
    private int roundTime;
    private int intermissionTime;

    private boolean ended;

    private boolean ranked;

    private ItemStack minigameLeaveItem;

    /**
     * Create a new CageMatch instance.
     *
     * @param arena
     * @param rounds
     * @param a
     * @param b
     */
    public CageMatch(CageArena arena, int rounds, Player a, Player b, boolean ranked) {
        this.uniqueId = UUID.randomUUID();
        this.arena = arena;
        this.rounds = new RoundResult[rounds];
        this.currentRound = 0;
        this.currentTime = 0;
        this.roundTime = 120;
        this.intermissionTime = 5;
        this.roundState = RoundState.INTERMISSION;
        this.ended = false;
        this.ranked = ranked;

        this.minigameLeaveItem = new ItemStack(Material.REDSTONE_BLOCK);

        /**
         * Run queries asynchronously to prevent the holding
         * up of the main thread. (Bukkit issues)
         */
        Bukkit.getScheduler().runTaskAsynchronously(BattlegroundBukkit.plugin(), () -> {
            final CageMatchStatistics aStatistics = CageMatchStatistics.DAO.getOrCreate(a.getUniqueId());
            final CageMatchStatistics bStatistics = CageMatchStatistics.DAO.getOrCreate(b.getUniqueId());

            /**
             * TODO: Load the player's kit
             */
            final BattlegroundKit aKit = new IronKit(a.getUniqueId())
                    .setMinigameLeaveItem(this.minigameLeaveItem);

            /**
             * TODO: Load the player's kit
             */
            final BattlegroundKit bKit = new IronKit(b.getUniqueId())
                    .setMinigameLeaveItem(this.minigameLeaveItem);

            /**
             * Run Bukkit-related tasks synchronously (Bukkit issues)
             */
            Bukkit.getScheduler().runTask(BattlegroundBukkit.plugin(), () -> {
                // Register the kit as an event listener
                Bukkit.getPluginManager().registerEvents(aKit, BattlegroundBukkit.plugin());
                Bukkit.getPluginManager().registerEvents(bKit, BattlegroundBukkit.plugin());

                this.a = new DataContainer(a.getUniqueId(), aStatistics, aKit);
                this.b = new DataContainer(b.getUniqueId(), bStatistics, bKit);

                Bukkit.getPluginManager().callEvent(new MinigameLoadedEvent(CageMatch.this));
            });
        });
    }

    @Override
    public long getDelay() {
        return 0;
    }

    @Override
    public long getInterval() {
        return 20;
    }

    @Override
    public CageArena getArena() {
        return this.arena;
    }

    @Override
    public ImmutableList<UUID> getPlayers() {
        ImmutableList.Builder<UUID> list = ImmutableList.builder();

        if (this.a != null) {
            list.add(this.a.getPlayer());
        }
        if (this.b != null) {
            list.add(this.b.getPlayer());
        }

        return list.build();
    }

    @Override
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    @Override
    public void run() {
        if (this.roundState.equals(RoundState.INTERMISSION)) {
            if (this.currentTime > 0) {
                this.currentTime--;
            } else {
                this.currentTime = this.roundTime;
                this.roundState = RoundState.PLAYING;
            }
        } else {
            if (this.currentTime > 0) {
                this.currentTime--;
            } else {
                this.endRound(new RoundResult());
            }
        }

        for (UUID uniqueId : this.getPlayers()) {
            Player p = Bukkit.getPlayer(uniqueId);
            if (p != null) {
                p.setLevel(this.currentTime);
            }
        }
    }

    /**
     * DataContainer
     */
    private class DataContainer {

        private long creationTime;
        private UUID uniqueId;
        private CageMatchStatistics statistics;
        private BattlegroundKit kit;
        private FancyScoreboard scoreboard;

        /**
         * Create a new DataContainer instance.
         *
         * @param uniqueId
         * @param statistics
         * @param kit
         */
        public DataContainer(UUID uniqueId, CageMatchStatistics statistics, BattlegroundKit kit) {
            this.creationTime = System.currentTimeMillis();
            this.uniqueId = uniqueId;
            this.statistics = statistics;
            this.kit = kit;
            this.scoreboard = new FancyScoreboard(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(Bukkit.getPlayer(uniqueId)), "minigames.cagematch.display-name"));
        }

        /**
         * Retrieve the player's unique id.
         *
         * @return
         */
        public UUID getPlayer() {
            return this.uniqueId;
        }

        /**
         * Set the container to hold the specified statistics.
         *
         * @param statistics
         */
        public void setStatistics(CageMatchStatistics statistics) {
            this.statistics = statistics;
        }

        /**
         * Retrieve the held statistics.
         *
         * @return
         */
        public Optional<CageMatchStatistics> getStatistics() {
            return Optional.ofNullable(this.statistics);
        }

        /**
         * Save the specified player's statistics.
         */
        public void saveStatistics(boolean ranked) {
            if (this.statistics != null) {
                if (ranked) {
                    this.statistics.setRankedKills(this.statistics.getRankedKills() + this.statistics.getKills());
                    this.statistics.setRankedDeaths(this.statistics.getRankedDeaths() + this.statistics.getDeaths());
                } else {
                    this.statistics.setUnrankedKills(this.statistics.getUnrankedKills() + this.statistics.getKills());
                    this.statistics.setUnrankedDeaths(this.statistics.getUnrankedDeaths() + this.statistics.getDeaths());
                }
                this.statistics.setTotalTimePlayed(this.statistics.getTotalTimePlayed() + (System.currentTimeMillis() - this.creationTime));

                if (BattlegroundBukkit.plugin().isServerStopping()) {
                    CageMatchStatistics.DAO.save(this.statistics);
                } else {
                    Bukkit.getScheduler().runTaskAsynchronously(BattlegroundBukkit.plugin(), () -> {
                        CageMatchStatistics.DAO.save(this.statistics);
                    });
                }
            }
        }

        /**
         * Set the container to hold the specified kit.
         *
         * @param kit
         */
        public void setKit(BattlegroundKit kit) {
            this.kit = kit;
        }

        /**
         * Retrieve the held kit.
         *
         * @return
         */
        public Optional<BattlegroundKit> getKit() {
            return Optional.ofNullable(this.kit);
        }

    }

    /**
     * Round state
     */
    private enum RoundState {
        INTERMISSION,
        PLAYING;
    }

    /**
     * Round result
     */
    private class RoundResult {

        private UUID winner;
        private UUID forfeiter;

        /**
         * Create a new RoundResult instance.
         */
        public RoundResult() {}

        /**
         * Create a new RoundResult instance.
         *
         * @param winner
         */
        public RoundResult(UUID winner) {
            this.winner = winner;
        }

        /**
         * Set the winner of the round.
         *
         * @param winner
         */
        public void setWinner(UUID winner) {
            this.winner = winner;
        }

        /**
         * Retrieve the winner of the round.
         *
         * @return
         */
        public Optional<UUID> getWinner() {
            return Optional.ofNullable(this.winner);
        }

        /**
         * Set the forfeiter of the round.
         *
         * @param forfeiter
         */
        public void setForfeiter(UUID forfeiter) {
            this.forfeiter = forfeiter;
        }

        /**
         * Get the forfeiter of the round.
         *
         * @return
         */
        public Optional<UUID> getForfeiter() {
            return Optional.ofNullable(this.forfeiter);
        }

    }

    /**
     * Set the data container of the player.
     *
     * @param container the player's statistics
     */
    public void setDataContainer(DataContainer container) {
        if (this.a.getPlayer().equals(container.getPlayer())) {
            this.a = container;
        }
        else if (this.b.getPlayer().equals(container.getPlayer())) {
            this.b = container;
        }
    }

    /**
     * Retrieve the statistics for a specified player.
     *
     * @param uniqueId The id of the player
     * @return An empty optional wrapper if the player's statistics could not be found
     */
    public Optional<DataContainer> getDataContainer(UUID uniqueId) {
        if (this.a.getPlayer().equals(uniqueId)) {
            return Optional.of(a);
        }
        else if (this.b.getPlayer().equals(uniqueId)) {
            return Optional.of(b);
        }
        return Optional.empty();
    }

    /**
     * Spawn the player.
     *
     * @param uniqueId The id of the player you wish to spawn
     */
    private Location spawn(UUID uniqueId) {
        Location location;
        boolean alternate = (this.currentRound % 2 == 0);

        if (this.a.getPlayer().equals(uniqueId)) {
            if (alternate) {
                location = this.getArena().getA();
            } else {
                location = this.getArena().getB();
            }
        } else if (this.b.getPlayer().equals(uniqueId)) {
            if (alternate) {
                location = this.getArena().getB();
            } else {
                location = this.getArena().getA();
            }
        } else {
            location = this.getArena().getSpawnLocation();
        }

        return location;
    }

    /**
     * Init the player.
     *
     * @param uniqueId The id of the player you wish to init
     */
    private void init(UUID uniqueId) {
        Player player = Bukkit.getPlayer(uniqueId);
        if (player != null) {
            player.setGameMode(GameMode.SURVIVAL);
            player.setHealth(20);
            player.setFoodLevel(20);
            player.setExp(0);
            player.setLevel(0);
            player.setFallDistance(0);
            player.setFireTicks(0);

            this.updateScoreboard(player);

            Optional<DataContainer> data = this.getDataContainer(player.getUniqueId());
            if (data.isPresent()) {
                Optional<BattlegroundKit> kit = data.get().getKit();
                if (kit.isPresent()) {
                    kit.get().give(player);
                }
            }
            this.updateScoreboard(player);
        }
    }

    /**
     * Update the player's scoreboard.
     *
     * @param player
     */
    private void updateScoreboard(Player player) {
        Locale locale = VoneBukkit.plugin().getPlayerLocale(player);

        Player aPlayer = Bukkit.getPlayer(this.a.getPlayer());
        Player bPlayer = Bukkit.getPlayer(this.b.getPlayer());

        FancyScoreboard s = new FancyScoreboard(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.display-name"))
                .reset()
                .blankLine()
                .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.scoreboard.round-message"))
                .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.scoreboard.round-score").replace("{{CURRENT_ROUND}}", String.valueOf(this.currentRound)).replace("{{TOTAL_ROUNDS}}", String.valueOf(this.rounds.length)))
                .blankLine();

        if (aPlayer != null) {
            s.add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.scoreboard.player-a-message").replace("{{PLAYER_NAME}}", aPlayer.getName()).replace("{{PLAYER_DISPLAY_NAME}}", aPlayer.getDisplayName()))
                    .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.scoreboard.player-a-score").replace("{{SCORE}}", String.valueOf(this.getScore(this.a.getPlayer()))))
                    .blankLine();
        }

        if (bPlayer != null) {
            s.add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.scoreboard.player-b-message").replace("{{PLAYER_NAME}}", bPlayer.getName()).replace("{{PLAYER_DISPLAY_NAME}}", bPlayer.getDisplayName()))
                    .add(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "minigames.cagematch.scoreboard.player-b-score").replace("{{SCORE}}", String.valueOf(this.getScore(this.b.getPlayer()))))
                    .blankLine();
        }

        s.build().send(player);
    }

    private String getFormattedTime() {
        long hours = TimeUnit.SECONDS.toHours(this.currentTime);
        long minutes = TimeUnit.SECONDS.toMinutes(this.currentTime);
        long seconds = this.currentTime / ((minutes > 1) ? minutes : 2);

        if (minutes > 0) {
            seconds = (seconds > 0) ? (seconds - 1) : 0;
        }

        String s = "";
        if (hours > 0) {
            s += String.format("%02d", hours) + ":";
        }

        s += String.format("%02d", minutes) + ":";
        s += String.format("%02d", seconds);

        return s;
    }

    /**
     * Retrieve a player's score.
     *
     * @param uniqueId
     * @return
     */
    public int getScore(UUID uniqueId) {
        int score = 0;
        for (RoundResult r : this.rounds) {
            if (r != null) {
                if (r.getWinner().isPresent()) {
                    if (r.getWinner().get().equals(uniqueId)) {
                        score++;
                    }
                }
            }
        }
        return score;
    }

    /**
     * Begin a new round.
     */
    private void startRound() {
        Player ap = Bukkit.getPlayer(this.a.getPlayer());
        Player bp = Bukkit.getPlayer(this.b.getPlayer());

        if (this.currentRound < this.rounds.length) {
            this.currentRound++;
            this.roundState = RoundState.INTERMISSION;
            this.currentTime = this.intermissionTime;
        }

        // Spawn player A if they are not dead
        if (ap != null) {
            if (!ap.isDead()) {
                ap.setFallDistance(0);
                ap.teleport(this.spawn(ap.getUniqueId()));
                this.init(ap.getUniqueId());
            }
        }

        // Spawn player B if they are not dead
        if (bp != null) {
            if (!bp.isDead()) {
                bp.setFallDistance(0);
                bp.teleport(this.spawn(bp.getUniqueId()));
                this.init(bp.getUniqueId());
            }
        }
    }

    /**
     * End the round.
     *
     * @param result
     */
    private void endRound(RoundResult result) {
        if (this.currentRound <= this.rounds.length) {
            this.rounds[this.currentRound - 1] = result;
        }

        if (result.getForfeiter().isPresent()) {
            this.endGame();
        } else {
            if (this.currentRound < this.rounds.length) {
                this.startRound();
            } else {
                this.endGame();
            }
        }
    }

    /**
     * End the game.
     */
    private void endGame() {
        this.ended = true;

        int aScore = this.getScore(this.a.getPlayer());
        int bScore = this.getScore(this.b.getPlayer());

        UUID forfeiter = null;

        int roundsPlayed = 0;
        for (RoundResult r : this.rounds) {
            if (r != null) {
                roundsPlayed++;
                forfeiter = r.getForfeiter().orElse(forfeiter);
            }
        }

        CageMatchGame game = CageMatchGame.DAO.getOrCreate(this.getUniqueId());
        game.setPlayerA(this.a.getPlayer());
        game.setPlayerB(this.b.getPlayer());
        game.setAScore(aScore);
        game.setBScore(bScore);
        game.setTotalRounds(this.rounds.length);
        game.setRoundsPlayed(roundsPlayed);
        game.setRanked(this.ranked);

        if (forfeiter != null) {
            if (this.a.getPlayer().equals(forfeiter)) {
                bWin();
            }
            else if (this.b.getPlayer().equals(forfeiter)) {
                aWin();
            }

            game.setForfeiter(forfeiter);
        } else {
            if (aScore > bScore) {
                aWin();
            }
            else if (bScore > aScore) {
                bWin();
            }
            else {
                draw();
            }
        }

        for (UUID uniqueId : this.getPlayers()) {
            Player p = Bukkit.getPlayer(uniqueId);
            if (p != null) {
                Bukkit.getPluginManager().callEvent(new MinigameQuitEvent(this, p));
            }
        }

        Bukkit.getScheduler().runTaskAsynchronously(BattlegroundBukkit.plugin(), () -> {
            CageMatchGame.DAO.save(game);
        });

        VoneBukkit.plugin().registry().unregister(this);
    }

    /**
     * Call this method to set the statistics if player A won.
     */
    private void aWin() {
        Optional<CageMatchStatistics> aStats = this.a.getStatistics();
        Optional<CageMatchStatistics> bStats = this.b.getStatistics();

        Player aPlayer = Bukkit.getPlayer(this.a.getPlayer());
        Player bPlayer = Bukkit.getPlayer(this.b.getPlayer());

        if (aStats.isPresent() && bStats.isPresent()) {
            if (this.ranked) {
                aStats.get().setRankedWins(aStats.get().getRankedWins() + 1);
                bStats.get().setRankedLoses(bStats.get().getRankedLoses() + 1);

                double aPointsBefore = aStats.get().getRating();
                double bPointsBefore = bStats.get().getRating();

                aStats.get().setRating(aStats.get().calculateScore(1, bStats.get()));
                bStats.get().setRating(bStats.get().calculateScore(0, aStats.get()));

                double aPoints = aStats.get().getRating() - aPointsBefore;
                double bPoints = bPointsBefore - bStats.get().getRating();

                if (aPlayer != null) {
                    this.sendWinMessage(aPlayer);
                    aPlayer.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(aPlayer), "minigames.cagematch.points-gained").replace("{{POINTS}}", String.format("%.2f", aPoints)));
                }

                if (bPlayer != null) {
                    this.sendLoseMessage(bPlayer);
                    bPlayer.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(bPlayer), "minigames.cagematch.points-lost").replace("{{POINTS}}", String.format("%.2f", bPoints)));
                }

            } else {
                aStats.get().setUnrankedWins(aStats.get().getUnrankedWins() + 1);
                bStats.get().setUnrankedLoses(bStats.get().getUnrankedLoses() + 1);

                if (aPlayer != null) {
                    this.sendWinMessage(aPlayer);
                }

                if (bPlayer != null) {
                    this.sendLoseMessage(bPlayer);
                }
            }
        }
    }

    /**
     * Call this method to set the statistics if player B won.
     */
    private void bWin() {
        Optional<CageMatchStatistics> aStats = this.a.getStatistics();
        Optional<CageMatchStatistics> bStats = this.b.getStatistics();

        Player aPlayer = Bukkit.getPlayer(this.a.getPlayer());
        Player bPlayer = Bukkit.getPlayer(this.b.getPlayer());

        if (aStats.isPresent() && bStats.isPresent()) {
            if (this.ranked) {
                aStats.get().setRankedLoses(aStats.get().getRankedLoses() + 1);
                bStats.get().setRankedWins(bStats.get().getRankedWins() + 1);

                double aPointsBefore = aStats.get().getRating();
                double bPointsBefore = bStats.get().getRating();

                aStats.get().setRating(aStats.get().calculateScore(0, bStats.get()));
                bStats.get().setRating(bStats.get().calculateScore(1, aStats.get()));

                double bPoints = bStats.get().getRating() - bPointsBefore;
                double aPoints = aPointsBefore - aStats.get().getRating();

                if (aPlayer != null) {
                    this.sendLoseMessage(aPlayer);
                    aPlayer.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(aPlayer), "minigames.cagematch.points-lost").replace("{{POINTS}}", String.format("%.2f", aPoints)));
                }

                if (bPlayer != null) {
                    this.sendWinMessage(bPlayer);
                    bPlayer.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(bPlayer), "minigames.cagematch.points-gained").replace("{{POINTS}}", String.format("%.2f", bPoints)));
                }

            } else {
                aStats.get().setUnrankedLoses(aStats.get().getUnrankedLoses() + 1);
                bStats.get().setUnrankedWins(bStats.get().getUnrankedWins() + 1);

                if (aPlayer != null) {
                    this.sendLoseMessage(aPlayer);
                }

                if (bPlayer != null) {
                    this.sendWinMessage(bPlayer);
                }
            }
        }
    }

    /**
     * Call this method to set the statistics if the match was a draw.
     */
    private void draw() {
        Optional<CageMatchStatistics> aStats = this.a.getStatistics();
        Optional<CageMatchStatistics> bStats = this.b.getStatistics();

        Player aPlayer = Bukkit.getPlayer(this.a.getPlayer());
        Player bPlayer = Bukkit.getPlayer(this.b.getPlayer());

        if (aStats.isPresent() && bStats.isPresent()) {
            if (this.ranked) {
                aStats.get().setRankedDraws(aStats.get().getRankedDraws() + 1);
                bStats.get().setRankedDraws(bStats.get().getRankedDraws() + 1);

                aStats.get().setRating(aStats.get().calculateScore(0.5, bStats.get()));
                bStats.get().setRating(bStats.get().calculateScore(0.5, aStats.get()));
            } else {
                aStats.get().setUnrankedDraws(aStats.get().getUnrankedDraws() + 1);
                bStats.get().setUnrankedDraws(bStats.get().getUnrankedDraws() + 1);
            }
        }

        if (aPlayer != null) {
            this.sendDrawMessage(aPlayer);
        }

        if (bPlayer != null) {
            this.sendDrawMessage(bPlayer);
        }
    }

    /**
     * Send the win message to the player.
     *
     * @param player
     */
    private void sendWinMessage(Player player) {
        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(player), "minigames.cagematch.you-won"));
    }

    /**
     * Send the lose message to the player.
     *
     * @param player
     */
    private void sendLoseMessage(Player player) {
        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(player), "minigames.cagematch.you-lost"));
    }

    /**
     * The message is sent to the player on draw.
     *
     * @param player
     */
    private void sendDrawMessage(Player player) {
        player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(VoneBukkit.plugin().getPlayerLocale(player), "minigames.cagematch.you-drew"));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onMinigameStarted(MinigameStartedEvent event) {
        if (event.getMinigame().equals(this)) {
            this.arena.getRegion().removeEntities(Item.class, Monster.class, Animals.class, Arrow.class);

            Bukkit.getScheduler().runTaskLater(BattlegroundBukkit.plugin(), () -> {
                CageMatch.this.startRound();

                for (UUID uniqueId : CageMatch.this.getPlayers()) {
                    Player player = Bukkit.getPlayer(uniqueId);
                    if (player != null) {
                        Bukkit.getPluginManager().callEvent(new MinigameJoinEvent(CageMatch.this, player));
                    } else {
                        RoundResult result = new RoundResult();
                        result.setForfeiter(uniqueId);
                        CageMatch.this.endRound(result);
                        break;
                    }
                }
            }, 5L);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onMinigameQuit(MinigameQuitEvent event) {
        if (event.getMinigame().equals(this)) {
            Optional<DataContainer> data = this.getDataContainer(event.getPlayer().getUniqueId());
            if (data.isPresent()) {
                data.get().saveStatistics(this.ranked);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerDeath(PlayerDeathEvent event) {
        event.setDeathMessage("");

        if (this.getPlayers().contains(event.getEntity().getUniqueId())) {
            Player victim = event.getEntity();

            event.setDroppedExp(0);
            event.getDrops().clear();

            // force respawn after 3s
            Bukkit.getScheduler().scheduleSyncDelayedTask(VoneBukkit.plugin(), () -> {
                victim.spigot().respawn();
            }, 20L * 3);

            // Statistics
            Optional<DataContainer> victimData = this.getDataContainer(victim.getUniqueId());

            if (victimData.isPresent()) {
                Optional<CageMatchStatistics> victimStats = victimData.get().getStatistics();

                if (victimStats.isPresent()) {
                    victimStats.get().setDeaths(victimStats.get().getDeaths() + 1);
                }
            }

            if (event.getEntity().getKiller() != null) {
                Player attacker = event.getEntity().getKiller();

                // If the player was killed by another player
                if (attacker != null && attacker.getUniqueId() != victim.getUniqueId() && this.getPlayers().contains(attacker.getUniqueId())) {

                    Locale victimLocale = VoneBukkit.plugin().getPlayerLocale(victim);
                    Locale attackerLocale = VoneBukkit.plugin().getPlayerLocale(attacker);

                    double attackerHealth = (attacker.getHealth() > 0) ? (attacker.getHealth() / 2) : 0;

                    // send message
                    String victimMessage = BattlegroundBukkit.plugin().i18n().translateDirect(victimLocale, "minigames.cagematch.killed-by")
                            .replace("{{ATTACKER_DISPLAY_NAME}}", attacker.getDisplayName())
                            .replace("{{HEARTS}}", String.format("%.2f", attackerHealth));

                    String attackerMessage = BattlegroundBukkit.plugin().i18n().translateDirect(victimLocale, "minigames.cagematch.you-killed")
                            .replace("{{VICTIM_DISPLAY_NAME}}", victim.getDisplayName());

                    victim.sendMessage(victimMessage);
                    attacker.sendMessage(attackerMessage);

                    Optional<DataContainer> attackerData = this.getDataContainer(attacker.getUniqueId());

                    if (attackerData.isPresent()) {
                        Optional<CageMatchStatistics> attackerStats = attackerData.get().getStatistics();

                        if (attackerStats.isPresent()) {
                            attackerStats.get().setKills(attackerStats.get().getKills() + 1);
                        }
                    }

                    this.updateScoreboard(attacker);
                }
            }

            this.updateScoreboard(event.getEntity());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerRespawn(PlayerRespawnEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            if (this.roundState.equals(RoundState.PLAYING)) {
                if (this.a.getPlayer().equals(event.getPlayer().getUniqueId())) {
                    this.endRound(new RoundResult(this.b.getPlayer()));
                }
                if (this.b.getPlayer().equals(event.getPlayer().getUniqueId())) {
                    this.endRound(new RoundResult(this.a.getPlayer()));
                }
            }

            if (this.ended) {
                Optional<BattlegroundLobby> optLobby = BattlegroundConnect.getInstance().getLobby();
                if (optLobby.isPresent()) {
                    event.setRespawnLocation(optLobby.get().getSpawnLocation());
                }
            } else {
                event.setRespawnLocation(this.spawn(event.getPlayer().getUniqueId()));

                Bukkit.getScheduler().runTaskLater(VoneBukkit.plugin(), () -> {
                    this.init(event.getPlayer().getUniqueId());
                }, 5L);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            if (this.getPlayers().contains(((Player) event.getEntity()).getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockIgnite(BlockIgniteEvent event) {
        if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
            if (event.getCause() == BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(BattlegroundBukkit.plugin(), () -> {
                    event.getBlock().setType(Material.AIR);
                }, 20L * 7);
            } else {
                if (event.getPlayer() != null) {
                    if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
                        event.setCancelled(true);
                    }
                } else {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBurn(BlockBurnEvent event) {
        if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockPlace(BlockPlaceEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            if (!event.getBlock().getType().equals(Material.FIRE)) {
                if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBreak(BlockBreakEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            if (this.getArena().getRegion().contains(event.getBlock().getLocation())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerMove(PlayerMoveEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            if (this.roundState.equals(RoundState.INTERMISSION)) {
                if (event.getFrom().getBlockX() != event.getTo().getBlockX() || event.getFrom().getBlockZ() != event.getTo().getBlockZ()) {
                    event.getPlayer().teleport(event.getFrom());
                }
                return;
            }

            if (!this.getArena().getRegion().contains(event.getTo())) {
                Location loc = event.getPlayer().getLocation();
                if (loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ()).getType().equals(Material.AIR)) {
                    if (!event.getPlayer().isDead() && this.roundState.equals(RoundState.PLAYING)) {
                        event.getPlayer().setHealth(0);
                    }
                } else {
                    event.getPlayer().teleport(event.getFrom());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerQuit(PlayerQuitEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            RoundResult r = new RoundResult();
            r.setForfeiter(event.getPlayer().getUniqueId());
            this.endRound(r);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerInteract(PlayerInteractEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                if (event.getPlayer().getInventory().getItemInHand().equals(this.minigameLeaveItem)) {
                    RoundResult r = new RoundResult();
                    r.setForfeiter(event.getPlayer().getUniqueId());
                    this.endRound(r);
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            if (event.getRightClicked().getType().equals(EntityType.ITEM_FRAME)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Player damager = null;
        boolean hitByProjectile = false;

        // If the round is in the intermission stage, ignore damage
        if (event.getEntity() instanceof Player) {
            if (this.getPlayers().contains(event.getEntity().getUniqueId()) && this.roundState.equals(RoundState.INTERMISSION)) {
                event.setCancelled(true);
                return;
            }
        }

        /**
         * If the damager is an instance of player
         */
        if (event.getDamager() instanceof Player) {
            damager = (Player) event.getDamager();
        }

        else if (event.getDamager() instanceof Projectile) {
            Projectile projectile = (Projectile) event.getDamager();
            hitByProjectile = true;
            if (projectile.getShooter() instanceof Player) {
                damager = (Player) projectile.getShooter();
            }
        }

        /**
         * If the victim entity is an ItemFrame
         */
        if (event.getEntity() instanceof ItemFrame) {
            if (damager != null) {
                if (this.getPlayers().contains(damager.getUniqueId())) {
                    event.setCancelled(true);
                }
            }
        }

        /**
         * If the victim entity is a Player
         */
        else if (event.getEntity() instanceof Player) {
            Player victim = (Player) event.getEntity();

            if (this.getPlayers().contains(victim.getUniqueId())) {
                if (damager != null && hitByProjectile) {
                    damager.playSound(damager.getLocation(), Sound.ORB_PICKUP, 1, 0);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onInventoryClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            if (this.getPlayers().contains(player.getUniqueId())) {
                if (event.getCurrentItem() != null) {
                    if (event.getCurrentItem().equals(this.minigameLeaveItem)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
        if (event.getRemover() instanceof Player) {
            Player player = (Player) event.getRemover();
            if (this.getPlayers().contains(player.getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onHangingPlace(HangingPlaceEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            ItemStack frame = event.getPlayer().getInventory().getItemInHand();
            int slot = event.getPlayer().getInventory().getHeldItemSlot();
            event.getPlayer().getInventory().setItem(slot, frame);
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onDrop(PlayerDropItemEvent event) {
        if (this.getPlayers().contains(event.getPlayer().getUniqueId())) {
            if (event.getItemDrop().getItemStack().equals(this.minigameLeaveItem)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onEntityExplode(EntityExplodeEvent event) {
        event.blockList().stream().forEach(block -> {
            if (this.getArena().getRegion().contains(block.getLocation())) {
                event.blockList().remove(block);
            }
        });
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onCreatureSpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.NATURAL)) {
            if (this.getArena().getRegion().contains(event.getLocation())) {
                /**
                 * If the entity is a monster
                 */
                if (event.getEntity() instanceof Monster) {
                    event.getEntity().remove();
                }

                /**
                 * If the entity is an animal
                 */
                else if (event.getEntity() instanceof Animals) {
                    event.getEntity().remove();
                }
            }
        }
    }

}
