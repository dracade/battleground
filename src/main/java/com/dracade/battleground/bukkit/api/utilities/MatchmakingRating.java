package com.dracade.battleground.bukkit.api.utilities;

public interface MatchmakingRating {

    /**
     * Set the player's rating.
     *
     * @param rating
     */
    void setRating(double rating);

    /**
     * Get the player's rating.
     *
     * @retur
     */
    double getRating();

    /**
     * Calculate the K-Factor.
     *
     * @return
     */
    default int kFactor() {
        int k = 0;

        // If the MMR rating is less than 2100
        if (this.getRating() < 2100) {
            k = 32;
        }

        // If the MMR rating is between 2100 and 2400
        else if (this.getRating() >= 2100 && this.getRating() <= 2400) {
            k = 24;
        }

        // If the MMR rating is over 2400
        else if (this.getRating() > 2400) {
            k = 16;
        }

        return k;
    }

    /**
     * Calculate the expected score.
     *
     * @param mmr The opposing player's matchmaking rating
     * @return
     */
    default double getExpectedScore(MatchmakingRating mmr) {
        return 1.0 / (1 + Math.pow(10, (mmr.getRating() - this.getRating()) / 400.0));
    }

    /**
     * Calculate the actual rating.
     *
     * @param score
     * @param mmr The opposing player's matchmaking rating
     * @return
     */
    default double calculateScore(double score, MatchmakingRating mmr) {
        return this.getRating() + this.kFactor() * (score - this.getExpectedScore(mmr));
    }

}
