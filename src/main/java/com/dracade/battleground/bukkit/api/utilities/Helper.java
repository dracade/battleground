package com.dracade.battleground.bukkit.api.utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility helper methods
 */
public class Helper {

    /**
     * Split's a string where newline characters are found.
     *
     * @param s
     * @return
     */
    public static List<String> splitWhereNewline(String s) {
        List<String> list = new ArrayList<String>();
        for (String i : s.split("\\s*\\r?\\\\n\\s*")) {
            list.add(i.trim());
        }
        return list;
    }

}
