package com.dracade.battleground.bukkit.api.menus.actions.minigame_menu;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.system.CageMatchQueue;
import com.dracade.battleground.bukkit.plugin.BattlegroundConnect;
import com.dracade.battleground.bukkit.plugin.BattlegroundLobby;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.game.Minigame;
import com.dracade.vone.bukkit.api.game.Playable;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;

public class MinigameJoinAction implements Runnable {

    private Minigame minigame;
    private Player player;

    /**
     * Create a new MinigameJoinAction.
     *
     * @param minigame
     */
    public MinigameJoinAction(Minigame minigame, Player player) {
        this.minigame = minigame;
        this.player = player;
    }

    /**
     * Retrieve the minigame to join.
     *
     * @return
     */
    public Minigame getMinigame() {
        return this.minigame;
    }

    /**
     * Retrieve the player.
     *
     * @return
     */
    public Player getPlayer() {
        return this.player;
    }

    @Override
    public void run() {
        if (CageMatchQueue.getInstance().contains(this.player.getUniqueId())) {
            Locale locale = VoneBukkit.plugin().getPlayerLocale(this.player);
            this.player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.cannot-join-game-in-queue"));
            this.player.closeInventory();
        } else {
            Optional<BattlegroundLobby> lobby = BattlegroundConnect.getInstance().getLobby();
            if (this.minigame instanceof Playable) {
                boolean cancelled = ((Playable) this.minigame).addPlayer(this.player.getUniqueId());
                if (!cancelled) {
                    if (lobby.isPresent()) {
                        lobby.get().removePlayer(this.player.getUniqueId());
                    }
                }
            }
        }
    }

}
