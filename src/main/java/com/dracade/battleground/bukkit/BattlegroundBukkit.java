package com.dracade.battleground.bukkit;

import com.blackypaw.mc.i18n.I18NUtilities;
import com.blackypaw.mc.i18n.Localizer;
import com.blackypaw.mc.i18n.TranslationStorage;
import com.blackypaw.mc.i18n.YamlTranslationStorage;
import com.dracade.battleground.bukkit.api.arenas.CageArena;
import com.dracade.battleground.bukkit.api.models.CageMatchGame;
import com.dracade.battleground.bukkit.api.models.CageMatchStatistics;
import com.dracade.battleground.bukkit.api.models.FreeForAllStatistics;
import com.dracade.battleground.bukkit.plugin.BattlegroundChat;
import com.dracade.battleground.bukkit.plugin.BattlegroundConnect;
import com.dracade.battleground.bukkit.api.arenas.OpenArena;
import com.dracade.battleground.bukkit.api.minigames.FreeForAll;
import com.dracade.battleground.bukkit.plugin.BattlegroundCommand;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.plugin.VoneCommand;
import com.dracade.vone.core.api.backend.VoneDatabase;
import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.List;
import java.util.Locale;

public class BattlegroundBukkit extends JavaPlugin implements Listener {

    private static BattlegroundBukkit instance;

    /**
     * Retrieve the battleground plugin.
     *
     * @return BattlegroundBukkit instance
     */
    public static BattlegroundBukkit plugin() {
        return BattlegroundBukkit.instance;
    }

    private Localizer localizer;
    private File translationsDirectory;
    private VoneDatabase database;

    private boolean serverStopping;

    /**
     * Create a new BattlegroundBukkit instance.
     *
     * @throws InstantiationException
     */
    public BattlegroundBukkit() throws InstantiationException {
        if (BattlegroundBukkit.instance != null) {
            throw new InstantiationException("BattlegroundBukkit is a singleton instance.");
        } else {
            /**
             * Instantiate the plugin
             */
            BattlegroundBukkit.instance = this;

            this.serverStopping = false;
        }
    }

    @Override
    public final void onEnable() {
        this.saveDefaultConfig();

        /**
         * Setup database
         */
        this.database = new VoneDatabase(this.getClassLoader()) {
            @Override
            protected List<Class<?>> getDatabaseClasses() {
                return Lists.newArrayList(
                        FreeForAllStatistics.class,
                        CageMatchStatistics.class,
                        CageMatchGame.class
                );
            };
        };

        try {
            this.database.initializeDatabase(
                    this.getConfig().getString("database.driver", "com.mysql.jdbc.Driver"),
                    this.getConfig().getString("database.url", "jdbc:mysql://127.0.0.1:3306/battleground"),
                    this.getConfig().getString("database.username", "root"),
                    this.getConfig().getString("database.password", ""),
                    this.getConfig().getString("database.isolation", "SERIALIZABLE"),
                    this.getConfig().getBoolean("database.logging", false),
                    this.getConfig().getBoolean("database.rebuild", true),
                    this.getDataFolder(),
                    this.getDescription().getName()
            );

            this.getConfig().set("database.rebuild", false);
            this.saveConfig();
        } catch (Exception e) {
            e.printStackTrace();
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Unable to connect to the database.");
        }

        /**
         * Setup translations
         */
        this.translationsDirectory = new File(this.getDataFolder(), "translations");
        this.translationsDirectory.mkdirs();
        if (!this.translationsDirectory.exists() || !new File(this.translationsDirectory, "en.yml").exists()) {
            InputStream defaultLocale = this.getResource("en.yml");
            OutputStream out;
            try {
                out = new FileOutputStream(new File(this.translationsDirectory, "en.yml"));
                byte[] buf = new byte[1024];
                int len;
                while((len = defaultLocale.read(buf)) > 0){
                    out.write(buf, 0, len);
                }
                out.close();
                defaultLocale.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        TranslationStorage translations = new YamlTranslationStorage(this.translationsDirectory);

        try {
            for (File file : this.translationsDirectory.listFiles()) {
                if (file.getName().endsWith(".yml")) {
                    String locale = file.getName().replace(".yml", "");
                    translations.loadLanguage(new Locale(locale));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.localizer = I18NUtilities.createLocalizer(translations);
        this.construct();
    }

    @Override
    public final void onDisable() {
        this.serverStopping = true;
        this.deconstruct();
    }

    /**
     * Retrieve the database.
     *
     * @return
     */
    public VoneDatabase getDatabaseHandler() {
        return this.database;
    }

    /**
     * Retrieve the localizer.
     *
     * @return The translator
     */
    public Localizer i18n() {
        return this.localizer;
    }

    /**
     * Check if the server is stopping.
     *
     * @return True if the server is stopping
     */
    public boolean isServerStopping() {
        return this.serverStopping;
    }

    /**
     * Check if the plugin is in API-Only mode.
     *
     * @return
     */
    public boolean isApiOnlyMode() {
        return BattlegroundBukkit.plugin().getConfig().getBoolean("api-only", false);
    }

    /**
     * This method will be called once BattlegroundBukkit has been enabled.
     */
    private void construct() {
        // Register commands
        this.getCommand(this.getDescription().getName()).setExecutor(BattlegroundCommand.instance());

        // Register arena editors
        VoneCommand.instance().register(OpenArena.CommandEditor.class);
        VoneCommand.instance().register(CageArena.CommandEditor.class);

        // Load BattlegroundConnect
        BattlegroundConnect.getInstance();

        if (this.getConfig().getBoolean("chat-format", true)) {
            Bukkit.getPluginManager().registerEvents(BattlegroundChat.getInstance(), BattlegroundBukkit.plugin());
        }
    }

    /**
     * This method will be called once BattlegroundBukkit has been disabled.
     */
    private void deconstruct() {
        BattlegroundConnect.getInstance().saveLobby();
        VoneBukkit.plugin().registry().getMinigames().stream().forEach(minigame -> {
            VoneBukkit.plugin().registry().unregister(minigame);
        });
    }

}
