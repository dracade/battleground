package com.dracade.battleground.bukkit.plugin;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.events.lobby.LobbyInitEvent;
import com.dracade.battleground.bukkit.api.events.lobby.LobbyJoinEvent;
import com.dracade.battleground.bukkit.api.events.lobby.LobbyQuitEvent;
import com.dracade.vone.bukkit.api.game.Region;
import com.dracade.vone.bukkit.api.serializer.Serializer;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;

import java.util.*;

public class BattlegroundLobby extends Serializer.Object implements Listener {

    private Location spawn;
    private Region region;
    private transient Set<UUID> players = new HashSet<UUID>();

    /**
     * Create a BattlegroundLobby instance.
     */
    private BattlegroundLobby() {
        this.region = new Region();
        this.players = new HashSet<UUID>();
    }

    /**
     * Create a BattlegroundLobby instance.
     *
     * @param spawn
     */
    public BattlegroundLobby(Location spawn) {
        this();
        Validate.notNull(spawn);

        this.spawn = spawn;
    }

    /**
     * Set the spawn location.
     *
     * @param spawn
     */
    public void setSpawnLocation(Location spawn) {
        Validate.notNull(spawn);

        this.spawn = spawn;
    }

    /**
     * Get the spawn location.
     *
     * @return
     */
    public Location getSpawnLocation() {
        return this.spawn;
    }

    /**
     * Set the region of the lobby.
     *
     * @param region
     */
    public void setRegion(Region region) {
        Validate.notNull(region);

        this.region = region;
    }

    /**
     * Retrieve the lobby region.
     *
     * @return
     */
    public Region getRegion() {
        return this.region;
    }

    /**
     * Init the player.
     *
     * @param player
     */
    public void init(Player player) {
        player.setGameMode(GameMode.ADVENTURE);
        player.setFoodLevel(20);
        player.setHealth(20);
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setLevel(0);
        player.setExp(0);

        Bukkit.getPluginManager().callEvent(new LobbyInitEvent(this, player));
    }

    /**
     * Add a player to the lobby.
     *
     * @param uniqueId The id of the player you wish to add
     */
    public boolean addPlayer(UUID uniqueId) {
        if (!this.containsPlayer(uniqueId)) {
            Player player = Bukkit.getPlayer(uniqueId);
            if (player != null) {
                LobbyJoinEvent event = new LobbyJoinEvent(this, player);
                Bukkit.getPluginManager().callEvent(event);
                if (!event.isCancelled()) {
                    this.players.add(player.getUniqueId());

                    player.teleport(this.getSpawnLocation());
                    this.init(player);
                }

                return !event.isCancelled();
            }
        }
        return (this.containsPlayer(uniqueId));
    }

    /**
     * Remove a player from the lobby.
     *
     * @param uniqueId The id of the player you wish to remove
     */
    public boolean removePlayer(UUID uniqueId) {
        if (this.containsPlayer(uniqueId)) {
            Player player = Bukkit.getPlayer(uniqueId);
            if (player != null) {
                LobbyQuitEvent event = new LobbyQuitEvent(this, player);
                Bukkit.getPluginManager().callEvent(event);

                if (!event.isCancelled()) {
                    this.players.remove(uniqueId);
                }

                return !event.isCancelled();
            }
        }
        return this.containsPlayer(uniqueId);
    }

    /**
     * Check if the lobby contains a player.
     *
     * @param uniqueId The id of the player you wish to check
     * @return
     */
    public boolean containsPlayer(UUID uniqueId) {
        return this.players.contains(uniqueId);
    }

    /**
     * Retrieve all of the players within the lobby.
     *
     * @return
     */
    public ImmutableList<UUID> getPlayers() {
        return ImmutableList.copyOf(this.players);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerRespawn(PlayerRespawnEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            event.setRespawnLocation(this.getSpawnLocation());
            Bukkit.getScheduler().runTaskLater(BattlegroundBukkit.plugin(), () -> {
                this.init(event.getPlayer());
            }, 10L);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerMove(PlayerMoveEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (!this.getRegion().contains(event.getTo())) {
                event.getPlayer().teleport(this.getSpawnLocation());
            }
        }
    }

    @EventHandler
    private void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            if (this.containsPlayer(((Player) event.getEntity()).getUniqueId())) {
                event.setCancelled(true);

                if (event.getEntity().getLocation().getY() < 0) {
                    event.getEntity().teleport(this.getSpawnLocation());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            if (this.containsPlayer(((Player) event.getEntity()).getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockIgnite(BlockIgniteEvent event) {
        if (this.getRegion().contains(event.getBlock().getLocation())) {
            if (event.getPlayer() != null) {
                if (this.containsPlayer(event.getPlayer().getUniqueId())) {
                    event.setCancelled(true);
                }
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBurn(BlockBurnEvent event) {
        if (this.getRegion().contains(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockPlace(BlockPlaceEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (this.getRegion().contains(event.getBlock().getLocation())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBreak(BlockBreakEvent event) {
        if (this.containsPlayer(event.getPlayer().getUniqueId())) {
            if (this.getRegion().contains(event.getBlock().getLocation())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onEntityExplode(EntityExplodeEvent event) {
        Optional<BattlegroundLobby> optLobby = BattlegroundConnect.getInstance().getLobby();
        if (optLobby.isPresent()) {
            event.blockList().stream().forEach(block -> {
                if (optLobby.get().getRegion().contains(block.getLocation())) {
                    event.blockList().remove(block);
                }
            });
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onCreatureSpawn(CreatureSpawnEvent event) {
        Optional<BattlegroundLobby> optLobby = BattlegroundConnect.getInstance().getLobby();
        if (optLobby.isPresent()) {
            if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.NATURAL)) {
                if (optLobby.get().getRegion().contains(event.getLocation())) {
                    /**
                     * If the entity is a monster
                     */
                    if (event.getEntity() instanceof Monster) {
                        event.getEntity().remove();
                    }

                    /**
                     * If the entity is an animal
                     */
                    else if (event.getEntity() instanceof Animals) {
                        event.getEntity().remove();
                    }
                }
            }
        }
    }

}
