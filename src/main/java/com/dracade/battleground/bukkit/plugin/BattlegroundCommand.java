package com.dracade.battleground.bukkit.plugin;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.arenas.CageArena;
import com.dracade.battleground.bukkit.api.minigames.CageMatch;
import com.dracade.battleground.bukkit.plugin.executors.LobbyExecutor;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.events.minigame.MinigameJoinEvent;
import com.dracade.vone.bukkit.api.exceptions.ArenaActiveException;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class BattlegroundCommand implements CommandExecutor, VoneExecutor {

    private static BattlegroundCommand instance;

    /**
     * Retrieve the BattlegroundCommand instance.
     *
     * @return BattlegroundCommand
     */
    public static BattlegroundCommand instance() {
        return (BattlegroundCommand.instance != null) ? BattlegroundCommand.instance : (BattlegroundCommand.instance = new BattlegroundCommand());
    }

    private VoneExecutor lobbyExecutor;

    /**
     * Create a new BattlegroundCommand instance.
     */
    private BattlegroundCommand() {
        this.lobbyExecutor = new LobbyExecutor();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (label.equalsIgnoreCase(BattlegroundBukkit.plugin().getDescription().getName())) {
            if (!this.execute(sender, args)) {
                sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.incorrect-command-usage"));
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        try {
            if (args.length > 0) {
                List<String> argsRemove = new ArrayList<String>(Arrays.asList(args));
                if (args.length > 0) {
                    argsRemove.remove(0);
                }

                String[] leftoverArguments = argsRemove.toArray(new String[argsRemove.size()]);

                if (args[0].equalsIgnoreCase("lobby")) {
                    return this.lobbyExecutor.execute(sender, leftoverArguments);
                }

            } else {
                return VoneBukkit.plugin().sendPluginInformation(BattlegroundBukkit.plugin(), BattlegroundBukkit.plugin().i18n(), sender);
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {}

        return false;
    }

}
