package com.dracade.battleground.bukkit.plugin;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.models.CageMatchStatistics;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.utilities.FancyMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import net.milkbowl.vault.chat.Chat;

import java.util.Locale;

public class BattlegroundChat implements Listener {

    private static BattlegroundChat instance;

    /**
     * Retrieve the BattlegroundChat instance.
     *
     * @return
     */
    public static BattlegroundChat getInstance() {
        return (BattlegroundChat.instance != null) ? BattlegroundChat.instance : (BattlegroundChat.instance = new BattlegroundChat());
    }

    private Chat chatService;

    /**
     * Create a new BattlegroundChat instance.
     */
    private BattlegroundChat() {
        this.chatService = BattlegroundBukkit.plugin().getServer().getServicesManager().getRegistration(Chat.class).getProvider();
    }

    /**
     * Send the message to the global chat.
     *
     * @param event
     * @param statistics
     */
    private void sendMessage(AsyncPlayerChatEvent event, CageMatchStatistics statistics) {
        Locale locale = VoneBukkit.plugin().getPlayerLocale(event.getPlayer());
        String message = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "settings.chat.format")
                .replace("{{MMR}}", String.valueOf((int) ((statistics != null) ? statistics.getRating() : 1000)))
                .replace("{{PREFIX}}", ChatColor.translateAlternateColorCodes('&', this.chatService.getPlayerPrefix(event.getPlayer())))
                .replace("{{SUFFIX}}", ChatColor.translateAlternateColorCodes('&', this.chatService.getPlayerSuffix(event.getPlayer())))
                .replace("{{SENDER_NAME}}", event.getPlayer().getName())
                .replace("{{SENDER_DISPLAY_NAME}}", event.getPlayer().getDisplayName())
                .replace("{{CHAT_MESSAGE}}", event.getMessage());

        event.setFormat(message);
    }

    @EventHandler
    private void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        // If the data is cached, then send
        if (CageMatchStatistics.DAO.isCached(event.getPlayer().getUniqueId())) {
            CageMatchStatistics stats = CageMatchStatistics.DAO.getOrCreate(event.getPlayer().getUniqueId());
            BattlegroundChat.this.sendMessage(event, stats);
        } else {
            // otherwise retrieve the data, then send with the next message
            Bukkit.getScheduler().runTaskAsynchronously(BattlegroundBukkit.plugin(), () -> {
                CageMatchStatistics.DAO.getOrCreate(event.getPlayer().getUniqueId());
            });

            this.sendMessage(event, null);
        }
    }

}
