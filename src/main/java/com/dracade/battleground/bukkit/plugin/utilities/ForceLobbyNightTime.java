package com.dracade.battleground.bukkit.plugin.utilities;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.plugin.BattlegroundLobby;
import org.bukkit.Bukkit;

import java.util.Optional;

/**
 * ForceLobbyNightTime
 *
 * this class forces the battleground lobby into
 * night time. It should be deleted and outsourced into
 * another plugin eventually, but it's here for now.
 *
 * TODO: remove.
 */
public class ForceLobbyNightTime {

    private static int taskId = -1;

    /**
     * Initiate the ForceLobbyNightTime.
     */
    public static void init(Optional<BattlegroundLobby> optLobby) {
        if (ForceLobbyNightTime.taskId > 0) {
            Bukkit.getScheduler().cancelTask(ForceLobbyNightTime.taskId);
        }

        if (optLobby.isPresent()) {
            ForceLobbyNightTime.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(BattlegroundBukkit.plugin(), () -> {
                optLobby.get().getSpawnLocation().getWorld().setStorm(false);
                optLobby.get().getSpawnLocation().getWorld().setTime(18000);
            }, 0L, 20L * 60);
        }
    }

    /**
     * Destroy the active task.
     */
    public static void destroy() {
        Bukkit.getScheduler().cancelTask(ForceLobbyNightTime.taskId);
    }

}
