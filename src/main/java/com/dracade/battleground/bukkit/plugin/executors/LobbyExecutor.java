package com.dracade.battleground.bukkit.plugin.executors;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.plugin.BattlegroundConnect;
import com.dracade.battleground.bukkit.plugin.BattlegroundLobby;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.system.VoneExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;
import java.util.Set;

public class LobbyExecutor implements VoneExecutor {

    @Override
    public boolean execute(CommandSender sender, String[] args) throws ArrayIndexOutOfBoundsException {
        final Locale locale;

        if (sender instanceof Player) {
            locale = VoneBukkit.plugin().getPlayerLocale((Player) sender);
        } else {
            locale = VoneBukkit.plugin().getDefaultLocale();
        }

        if (sender instanceof Player) {
            Player player = ((Player) sender);
            Optional<BattlegroundLobby> lobby = BattlegroundConnect.getInstance().getLobby();
            if (lobby.isPresent()) {

                /**
                 * Join the lobby
                 */
                if (args[0].equalsIgnoreCase("join")) {
                    if (sender.hasPermission("battleground.lobby.join")) {
                        if (!lobby.get().containsPlayer(player.getUniqueId())) {
                            lobby.get().addPlayer(player.getUniqueId());
                            sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.lobby.joined"));
                        } else {
                            sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.lobby.already-in-lobby"));
                        }
                    } else {
                        sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
                    }
                }

                /**
                 * Leave the lobby
                 */
                else if (args[0].equalsIgnoreCase("leave")) {
                    if (sender.hasPermission("battleground.lobby.leave")) {
                        if (lobby.get().containsPlayer(player.getUniqueId())) {
                            lobby.get().removePlayer(player.getUniqueId());
                            sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.lobby.quit"));
                        } else {
                            sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.lobby.not-in-lobby"));
                        }
                    } else {
                        sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
                    }
                }

                /**
                 * Edit ...
                 */
                else if (args[0].equalsIgnoreCase("edit")) {
                    if (sender.hasPermission("battleground.lobby.edit")) {

                        /**
                         * Set ...
                         */
                        if (args[1].equalsIgnoreCase("set")) {

                            /**
                             * Set the lobby spawn location
                             */
                            if (args[2].equalsIgnoreCase("spawn")) {
                                lobby.get().setSpawnLocation(player.getLocation());
                                sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.lobby.spawn-set"));
                            }

                            /**
                             * Set the lobby spawn location
                             */
                            else if (args[2].equalsIgnoreCase("region")) {

                                /**
                                 * Set the container A value
                                 */
                                if (args[3].equalsIgnoreCase("a")) {
                                    lobby.get().getRegion().setA(player.getTargetBlock((Set) null, 5).getLocation());
                                    player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.lobby.region-a-set"));
                                }

                                /**
                                 * Set the container B value
                                 */
                                else if (args[3].equalsIgnoreCase("b")) {
                                    lobby.get().getRegion().setB(player.getTargetBlock((Set) null, 5).getLocation());
                                    player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.lobby.region-b-set"));
                                }

                                /**
                                 * Set the container to ignore y axis values
                                 */
                                else if (args[3].equalsIgnoreCase("ignore-y")) {
                                    lobby.get().getRegion().setIgnoreY(!lobby.get().getRegion().isIgnoreY());
                                    player.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.informative.lobby.region-ignore-y-set").replace("{{VALUE}}", String.valueOf(lobby.get().getRegion().isIgnoreY())));
                                }

                                /**
                                 * Incorrect usage
                                 */
                                else {
                                    return false;
                                }

                            }

                            /**
                             * Incorrect usage
                             */
                            else {
                                return false;
                            }

                        }

                        /**
                         * Incorrect usage
                         */
                        else {
                            return false;
                        }

                    } else {
                        sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.no-permission"));
                    }

                }

                /**
                 * Incorrect usage
                 */
                else {
                    return false;
                }

            }

            /**
             * BattlegroundLobby not found.
             */
            else {
                sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.lobby.not-found"));
            }

        } else {
            sender.sendMessage(BattlegroundBukkit.plugin().i18n().translateDirect(locale, "messages.errors.player-only-command"));
        }

        return true;
    }

}
