package com.dracade.battleground.bukkit.plugin;

import com.dracade.battleground.bukkit.BattlegroundBukkit;
import com.dracade.battleground.bukkit.api.events.lobby.LobbyInitEvent;
import com.dracade.battleground.bukkit.api.menus.MinigameMenu;
import com.dracade.battleground.bukkit.api.menus.NavigationMenu;
import com.dracade.battleground.bukkit.api.models.CageMatchStatistics;
import com.dracade.battleground.bukkit.api.system.CageMatchQueue;
import com.dracade.battleground.bukkit.api.system.CageMatchQueueItem;
import com.dracade.battleground.bukkit.plugin.utilities.ForceLobbyNightTime;
import com.dracade.battleground.bukkit.api.arenas.OpenArena;
import com.dracade.battleground.bukkit.api.minigames.FreeForAll;
import com.dracade.vone.bukkit.VoneBukkit;
import com.dracade.vone.bukkit.api.events.minigame.MinigameJoinEvent;
import com.dracade.vone.bukkit.api.events.minigame.MinigameQuitEvent;
import com.dracade.vone.bukkit.api.events.queue.QueueAddEvent;
import com.dracade.vone.bukkit.api.exceptions.ArenaActiveException;
import com.dracade.vone.bukkit.api.system.VoneQueue;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class BattlegroundConnect implements Listener {

    private static BattlegroundConnect instance;

    /**
     * Retrieve the BattlegroundConnect instance.
     *
     * @return
     */
    public static BattlegroundConnect getInstance() {
        return (BattlegroundConnect.instance != null) ? BattlegroundConnect.instance : (BattlegroundConnect.instance = new BattlegroundConnect());
    }

    private BattlegroundLobby lobby;
    private File lobbyFile;

    private ItemStack finder;

    /**
     * Create a new BattlegroundConnect instance.
     */
    private BattlegroundConnect() {
        this.lobbyFile = new File(BattlegroundBukkit.plugin().getDataFolder(), "lobby.json");
        this.setGameFinderItem(new ItemStack(Material.COMPASS));

        // Create the lobby from file
        if (this.lobbyFile.exists()) {
            Optional<BattlegroundLobby> optLobby = VoneBukkit.plugin().serializer().fromJson(this.lobbyFile, BattlegroundLobby.class);
            if (optLobby.isPresent()) {
                this.setLobby(optLobby.get());
            }
        }

        // Set a default lobby if one doesn't exist
        if (!this.getLobby().isPresent()) {
            this.setLobby(new BattlegroundLobby(Bukkit.getWorlds().get(0).getSpawnLocation()));
            this.saveLobby();
        }

        /**
         * If api-only mode isn't enabled, then we register the "connect" listeners.
         */
        if (!BattlegroundBukkit.plugin().isApiOnlyMode()) {
            this.register();
        } else {
            this.unregister();
        }
    }

    /**
     * Register the lobby handlers.
     */
    private void register() {
        // Add all players to lobby on load
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            Bukkit.getOnlinePlayers().stream().forEach(player -> {
                optLobby.get().addPlayer(player.getUniqueId());
            });
        }

        // Register listeners
        Bukkit.getPluginManager().registerEvents(this, BattlegroundBukkit.plugin());

        // Setup the lobby
        ForceLobbyNightTime.init(this.getLobby());

        // Start all of the minigames
        VoneBukkit.plugin().registry().getArenas().stream().forEach(a -> {
            if (a instanceof OpenArena) {
                try {
                    VoneBukkit.plugin().registry().register(new FreeForAll((OpenArena) a));
                } catch (ArenaActiveException e) {
                    Bukkit.getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + e.getArena().getName() + " is already active.");
                }
            }
        });
    }

    /**
     * Unregister the lobby handlers.
     */
    private void unregister() {
        ForceLobbyNightTime.destroy();
        HandlerList.unregisterAll(this);
    }

    /**
     * Set the game finder item.
     *
     * @param finder
     * @return
     */
    private void setGameFinderItem(ItemStack finder) {
        this.finder = finder;
    }

    /**
     * Retrieve the game finder item.
     *
     * @param locale
     * @return
     */
    public Optional<ItemStack> getGameFinderItem(Locale locale) {
        if (this.finder != null) {
            ItemMeta itemMeta = this.finder.getItemMeta();

            String itemDisplayName = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "lobby.display-name");
            String itemLore = BattlegroundBukkit.plugin().i18n().translateDirect(locale, "lobby.display-lore");

            if (!itemDisplayName.isEmpty()) {
                itemMeta.setDisplayName(itemDisplayName);
            }

            if (!itemLore.isEmpty()) {
                List<String> itemLoreList = new ArrayList<String>();
                for (String s : itemLore.split("\\s*\\r?\\\\n\\s*")) {
                    itemLoreList.add(s.trim());
                }
                itemMeta.setLore(itemLoreList);
            }

            itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            this.finder.setItemMeta(itemMeta);

            return Optional.of(this.finder);
        } else {
            return Optional.empty();
        }
    }

    /**
     * Save the BattlegroundLobby.
     *
     * @param lobby
     */
    public void setLobby(BattlegroundLobby lobby) {
        Optional<BattlegroundLobby> oldLobby = this.getLobby();
        if (oldLobby.isPresent()) {
            HandlerList.unregisterAll(oldLobby.get());
        }

        this.lobby = lobby;

        Bukkit.getPluginManager().registerEvents(this.lobby, BattlegroundBukkit.plugin());
    }

    /**
     * Retrieve the BattlegroundLobby.
     *
     * @return
     */
    public Optional<BattlegroundLobby> getLobby() {
        return (this.lobby != null) ? Optional.of(this.lobby) : Optional.empty();
    }

    /**
     * Save the BattlegroundLobby.
     */
    public void saveLobby() {
        if (this.lobby != null) {
            try {
                if (!this.lobbyFile.exists()) {
                    this.lobbyFile.createNewFile();
                }
                VoneBukkit.plugin().serializer().toJson(this.lobby, this.lobbyFile);
            } catch (IOException e) {
                Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "The lobby failed to save.");
            }
        } else {
            this.lobbyFile.delete();
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerJoin(PlayerJoinEvent event) {
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            optLobby.get().addPlayer(event.getPlayer().getUniqueId());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerQuit(PlayerQuitEvent event) {
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            optLobby.get().removePlayer(event.getPlayer().getUniqueId());
        }

        // Remove the player from any queues.
        CageMatchQueue.getInstance().remove(event.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onMinigameJoin(MinigameJoinEvent event) {
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            optLobby.get().removePlayer(event.getPlayer().getUniqueId());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onMinigameQuit(MinigameQuitEvent event) {
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            optLobby.get().addPlayer(event.getPlayer().getUniqueId());
            event.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onLobbyInit(LobbyInitEvent event) {
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            if (event.getLobby().equals(optLobby.get())) {
                if (event.getLobby().containsPlayer(event.getPlayer().getUniqueId())) {
                    Locale locale = VoneBukkit.plugin().getPlayerLocale(event.getPlayer());
                    Optional<ItemStack> finder = this.getGameFinderItem(locale);
                    if (finder.isPresent()) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(BattlegroundBukkit.plugin(), () -> {
                            event.getPlayer().getInventory().setItem(0, finder.get());
                            event.getPlayer().getInventory().setHeldItemSlot(0);
                            event.getPlayer().updateInventory();
                        }, 5L);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onPlayerRespawn(PlayerRespawnEvent event) {
        Optional<BattlegroundLobby> optLobby = BattlegroundConnect.getInstance().getLobby();
        if (optLobby.isPresent() && optLobby.get().containsPlayer(event.getPlayer().getUniqueId())) {
            event.setRespawnLocation(optLobby.get().getSpawnLocation());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onInteract(PlayerInteractEvent event) {
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            if (optLobby.get().containsPlayer(event.getPlayer().getUniqueId())) {
                Locale locale = VoneBukkit.plugin().getPlayerLocale(event.getPlayer());

                if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                    Optional<ItemStack> finder = this.getGameFinderItem(locale);
                    if (finder.isPresent()) {
                        if (event.getPlayer().getInventory().getItemInHand().equals(finder.get())) {
                            new NavigationMenu().open(event.getPlayer());
                        }
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onDrop(PlayerDropItemEvent event) {
        Optional<BattlegroundLobby> optLobby = this.getLobby();
        if (optLobby.isPresent()) {
            if (optLobby.get().containsPlayer(event.getPlayer().getUniqueId())) {
                if (event.getItemDrop().getItemStack().equals(this.finder)) {
                    event.setCancelled(true);
                }
            }
        }
    }

}
